﻿using System;
using System.IO;
using System.Xml.Linq;
using System.Xml.Serialization;
using Central;

namespace RepositoryTypesCommonProject
{
    public class ConcreteTypeRepo : IRepo<Product>
    {
        private readonly string FilePath = "DummyDB.xml";

        public ConcreteTypeRepo()
        {

        }

        public ConcreteTypeRepo(string path)
        {
            FilePath = path;
        }

        public void Persist(params Product[] elem)
        {
            var xs = new XmlSerializer(
                typeof(TY),
                new Type[] { typeof(TY), typeof(TX) }
            );

            using( var stream = new FileStream(FilePath, FileMode.Create))
            {
                xs.Serialize(stream, elem);
            }
            
        }

        public void Load(out TZ output)
        {
            //TZ fprod = null;

            XmlSerializer xs = new XmlSerializer(
                typeof(TZ), 
                new Type[]
                    {
                        typeof(TY), 
                        typeof(TX)
                    }
            );

            FileStream stream = new FileStream(fileName, FileMode.Open);
            output = (TZ) xs.Deserialize(stream);
            if (fprod != null)
            {
                Console.WriteLine(fprod.Name + " - " + fprod.Tipfam);
                foreach (Produto p in fprod.Lprod)
                {
                    Console.WriteLine(p.Codigo + ":" + p.Designacao + ":" + p.Price);
                }
            }
            stream.Close();
        }
    }
}
