﻿namespace RepositoryTypesCommonProject
{
    public interface IRepo<T>
    {
        /// <summary>
        /// </summary>
        /// <param name="elem"></param>
        void Persist(params T[] elem);
    }
}
