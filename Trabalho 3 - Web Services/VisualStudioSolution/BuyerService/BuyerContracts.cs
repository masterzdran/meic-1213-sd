﻿using System;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace BuyerProject
{
    [ServiceContract]
    interface IBuyerContract
    {
        [OperationContract]
        bool SubmitSellingProposal(BuyerData data);
    }

    [DataContract]
    public class BuyerData
    {
        [DataMember]
        public int Id { get; set; }

        [DataMember]
        public Uri Uri { get; set; }

        [DataMember]
        public float Value { get; set; }
    }
}
