﻿using System;
using System.ServiceModel;
using BuyerProject.ConcreteSupplierServiceReference;

namespace BuyerProject
{

    [ServiceBehavior(
        ConcurrencyMode = ConcurrencyMode.Multiple, 
        InstanceContextMode = InstanceContextMode.Single
    )]
    class ConcreteBuyerService : IBuyerContract
    {
        public delegate bool SubmitSellingProposalDelegate(BuyerData data);
        private event SubmitSellingProposalDelegate _submitSellingProposalEvent;

        public ConcreteBuyerService(SubmitSellingProposalDelegate method)
        {
            _submitSellingProposalEvent += method;
        }

        public bool SubmitSellingProposal(BuyerData data)
        {
            return _submitSellingProposalEvent.Invoke(data);
        }
    }
}
