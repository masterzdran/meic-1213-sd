﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18010
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BuyerProject.CentralServiceReference {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ProductFamilly", Namespace="http://schemas.datacontract.org/2004/07/CentralProject")]
    public enum ProductFamilly : int {
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Hardware = 0,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Software = 1,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Servicos = 2,
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="SubmissionFault", Namespace="http://schemas.datacontract.org/2004/07/CentralProject")]
    [System.SerializableAttribute()]
    public partial class SubmissionFault : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private string DetailsField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private BuyerProject.CentralServiceReference.ProductFamilly SupplierFamillyField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public string Details {
            get {
                return this.DetailsField;
            }
            set {
                if ((object.ReferenceEquals(this.DetailsField, value) != true)) {
                    this.DetailsField = value;
                    this.RaisePropertyChanged("Details");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public BuyerProject.CentralServiceReference.ProductFamilly SupplierFamilly {
            get {
                return this.SupplierFamillyField;
            }
            set {
                if ((this.SupplierFamillyField.Equals(value) != true)) {
                    this.SupplierFamillyField = value;
                    this.RaisePropertyChanged("SupplierFamilly");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="CentralComposite", Namespace="http://schemas.datacontract.org/2004/07/CentralProject")]
    [System.SerializableAttribute()]
    public partial class CentralComposite : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int AcquirementProposalIdField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private System.Uri[] SuppliersField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int AcquirementProposalId {
            get {
                return this.AcquirementProposalIdField;
            }
            set {
                if ((this.AcquirementProposalIdField.Equals(value) != true)) {
                    this.AcquirementProposalIdField = value;
                    this.RaisePropertyChanged("AcquirementProposalId");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public System.Uri[] Suppliers {
            get {
                return this.SuppliersField;
            }
            set {
                if ((object.ReferenceEquals(this.SuppliersField, value) != true)) {
                    this.SuppliersField = value;
                    this.RaisePropertyChanged("Suppliers");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="CentralServiceReference.IManageSupplier", CallbackContract=typeof(BuyerProject.CentralServiceReference.IManageSupplierCallback), SessionMode=System.ServiceModel.SessionMode.Required)]
    public interface IManageSupplier {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IManageSupplier/RegisterSupplier", ReplyAction="http://tempuri.org/IManageSupplier/RegisterSupplierResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(BuyerProject.CentralServiceReference.SubmissionFault), Action="http://tempuri.org/IManageSupplier/RegisterSupplierSubmissionFaultFault", Name="SubmissionFault", Namespace="http://schemas.datacontract.org/2004/07/CentralProject")]
        void RegisterSupplier(BuyerProject.CentralServiceReference.ProductFamilly productFamilly, System.Uri uri);
        
        [System.ServiceModel.OperationContractAttribute(AsyncPattern=true, Action="http://tempuri.org/IManageSupplier/RegisterSupplier", ReplyAction="http://tempuri.org/IManageSupplier/RegisterSupplierResponse")]
        System.IAsyncResult BeginRegisterSupplier(BuyerProject.CentralServiceReference.ProductFamilly productFamilly, System.Uri uri, System.AsyncCallback callback, object asyncState);
        
        void EndRegisterSupplier(System.IAsyncResult result);
        
        [System.ServiceModel.OperationContractAttribute(IsTerminating=true, IsInitiating=false, Action="http://tempuri.org/IManageSupplier/UnregisterSupplier", ReplyAction="http://tempuri.org/IManageSupplier/UnregisterSupplierResponse")]
        [System.ServiceModel.FaultContractAttribute(typeof(BuyerProject.CentralServiceReference.SubmissionFault), Action="http://tempuri.org/IManageSupplier/UnregisterSupplierSubmissionFaultFault", Name="SubmissionFault", Namespace="http://schemas.datacontract.org/2004/07/CentralProject")]
        void UnregisterSupplier(BuyerProject.CentralServiceReference.ProductFamilly productFamilly, System.Uri uri);
        
        [System.ServiceModel.OperationContractAttribute(IsTerminating=true, IsInitiating=false, AsyncPattern=true, Action="http://tempuri.org/IManageSupplier/UnregisterSupplier", ReplyAction="http://tempuri.org/IManageSupplier/UnregisterSupplierResponse")]
        System.IAsyncResult BeginUnregisterSupplier(BuyerProject.CentralServiceReference.ProductFamilly productFamilly, System.Uri uri, System.AsyncCallback callback, object asyncState);
        
        void EndUnregisterSupplier(System.IAsyncResult result);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IManageSupplierCallback {
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, Action="http://tempuri.org/IManageSupplier/NotifyProposal")]
        void NotifyProposal(int acquirementProposalId, System.Uri uriBuyer);
        
        [System.ServiceModel.OperationContractAttribute(IsOneWay=true, AsyncPattern=true, Action="http://tempuri.org/IManageSupplier/NotifyProposal")]
        System.IAsyncResult BeginNotifyProposal(int acquirementProposalId, System.Uri uriBuyer, System.AsyncCallback callback, object asyncState);
        
        void EndNotifyProposal(System.IAsyncResult result);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IManageSupplierChannel : BuyerProject.CentralServiceReference.IManageSupplier, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class ManageSupplierClient : System.ServiceModel.DuplexClientBase<BuyerProject.CentralServiceReference.IManageSupplier>, BuyerProject.CentralServiceReference.IManageSupplier {
        
        private BeginOperationDelegate onBeginRegisterSupplierDelegate;
        
        private EndOperationDelegate onEndRegisterSupplierDelegate;
        
        private System.Threading.SendOrPostCallback onRegisterSupplierCompletedDelegate;
        
        private BeginOperationDelegate onBeginUnregisterSupplierDelegate;
        
        private EndOperationDelegate onEndUnregisterSupplierDelegate;
        
        private System.Threading.SendOrPostCallback onUnregisterSupplierCompletedDelegate;
        
        public ManageSupplierClient(System.ServiceModel.InstanceContext callbackInstance) : 
                base(callbackInstance) {
        }
        
        public ManageSupplierClient(System.ServiceModel.InstanceContext callbackInstance, string endpointConfigurationName) : 
                base(callbackInstance, endpointConfigurationName) {
        }
        
        public ManageSupplierClient(System.ServiceModel.InstanceContext callbackInstance, string endpointConfigurationName, string remoteAddress) : 
                base(callbackInstance, endpointConfigurationName, remoteAddress) {
        }
        
        public ManageSupplierClient(System.ServiceModel.InstanceContext callbackInstance, string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(callbackInstance, endpointConfigurationName, remoteAddress) {
        }
        
        public ManageSupplierClient(System.ServiceModel.InstanceContext callbackInstance, System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(callbackInstance, binding, remoteAddress) {
        }
        
        public event System.EventHandler<System.ComponentModel.AsyncCompletedEventArgs> RegisterSupplierCompleted;
        
        public event System.EventHandler<System.ComponentModel.AsyncCompletedEventArgs> UnregisterSupplierCompleted;
        
        public void RegisterSupplier(BuyerProject.CentralServiceReference.ProductFamilly productFamilly, System.Uri uri) {
            base.Channel.RegisterSupplier(productFamilly, uri);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        public System.IAsyncResult BeginRegisterSupplier(BuyerProject.CentralServiceReference.ProductFamilly productFamilly, System.Uri uri, System.AsyncCallback callback, object asyncState) {
            return base.Channel.BeginRegisterSupplier(productFamilly, uri, callback, asyncState);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        public void EndRegisterSupplier(System.IAsyncResult result) {
            base.Channel.EndRegisterSupplier(result);
        }
        
        private System.IAsyncResult OnBeginRegisterSupplier(object[] inValues, System.AsyncCallback callback, object asyncState) {
            BuyerProject.CentralServiceReference.ProductFamilly productFamilly = ((BuyerProject.CentralServiceReference.ProductFamilly)(inValues[0]));
            System.Uri uri = ((System.Uri)(inValues[1]));
            return this.BeginRegisterSupplier(productFamilly, uri, callback, asyncState);
        }
        
        private object[] OnEndRegisterSupplier(System.IAsyncResult result) {
            this.EndRegisterSupplier(result);
            return null;
        }
        
        private void OnRegisterSupplierCompleted(object state) {
            if ((this.RegisterSupplierCompleted != null)) {
                InvokeAsyncCompletedEventArgs e = ((InvokeAsyncCompletedEventArgs)(state));
                this.RegisterSupplierCompleted(this, new System.ComponentModel.AsyncCompletedEventArgs(e.Error, e.Cancelled, e.UserState));
            }
        }
        
        public void RegisterSupplierAsync(BuyerProject.CentralServiceReference.ProductFamilly productFamilly, System.Uri uri) {
            this.RegisterSupplierAsync(productFamilly, uri, null);
        }
        
        public void RegisterSupplierAsync(BuyerProject.CentralServiceReference.ProductFamilly productFamilly, System.Uri uri, object userState) {
            if ((this.onBeginRegisterSupplierDelegate == null)) {
                this.onBeginRegisterSupplierDelegate = new BeginOperationDelegate(this.OnBeginRegisterSupplier);
            }
            if ((this.onEndRegisterSupplierDelegate == null)) {
                this.onEndRegisterSupplierDelegate = new EndOperationDelegate(this.OnEndRegisterSupplier);
            }
            if ((this.onRegisterSupplierCompletedDelegate == null)) {
                this.onRegisterSupplierCompletedDelegate = new System.Threading.SendOrPostCallback(this.OnRegisterSupplierCompleted);
            }
            base.InvokeAsync(this.onBeginRegisterSupplierDelegate, new object[] {
                        productFamilly,
                        uri}, this.onEndRegisterSupplierDelegate, this.onRegisterSupplierCompletedDelegate, userState);
        }
        
        public void UnregisterSupplier(BuyerProject.CentralServiceReference.ProductFamilly productFamilly, System.Uri uri) {
            base.Channel.UnregisterSupplier(productFamilly, uri);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        public System.IAsyncResult BeginUnregisterSupplier(BuyerProject.CentralServiceReference.ProductFamilly productFamilly, System.Uri uri, System.AsyncCallback callback, object asyncState) {
            return base.Channel.BeginUnregisterSupplier(productFamilly, uri, callback, asyncState);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        public void EndUnregisterSupplier(System.IAsyncResult result) {
            base.Channel.EndUnregisterSupplier(result);
        }
        
        private System.IAsyncResult OnBeginUnregisterSupplier(object[] inValues, System.AsyncCallback callback, object asyncState) {
            BuyerProject.CentralServiceReference.ProductFamilly productFamilly = ((BuyerProject.CentralServiceReference.ProductFamilly)(inValues[0]));
            System.Uri uri = ((System.Uri)(inValues[1]));
            return this.BeginUnregisterSupplier(productFamilly, uri, callback, asyncState);
        }
        
        private object[] OnEndUnregisterSupplier(System.IAsyncResult result) {
            this.EndUnregisterSupplier(result);
            return null;
        }
        
        private void OnUnregisterSupplierCompleted(object state) {
            if ((this.UnregisterSupplierCompleted != null)) {
                InvokeAsyncCompletedEventArgs e = ((InvokeAsyncCompletedEventArgs)(state));
                this.UnregisterSupplierCompleted(this, new System.ComponentModel.AsyncCompletedEventArgs(e.Error, e.Cancelled, e.UserState));
            }
        }
        
        public void UnregisterSupplierAsync(BuyerProject.CentralServiceReference.ProductFamilly productFamilly, System.Uri uri) {
            this.UnregisterSupplierAsync(productFamilly, uri, null);
        }
        
        public void UnregisterSupplierAsync(BuyerProject.CentralServiceReference.ProductFamilly productFamilly, System.Uri uri, object userState) {
            if ((this.onBeginUnregisterSupplierDelegate == null)) {
                this.onBeginUnregisterSupplierDelegate = new BeginOperationDelegate(this.OnBeginUnregisterSupplier);
            }
            if ((this.onEndUnregisterSupplierDelegate == null)) {
                this.onEndUnregisterSupplierDelegate = new EndOperationDelegate(this.OnEndUnregisterSupplier);
            }
            if ((this.onUnregisterSupplierCompletedDelegate == null)) {
                this.onUnregisterSupplierCompletedDelegate = new System.Threading.SendOrPostCallback(this.OnUnregisterSupplierCompleted);
            }
            base.InvokeAsync(this.onBeginUnregisterSupplierDelegate, new object[] {
                        productFamilly,
                        uri}, this.onEndUnregisterSupplierDelegate, this.onUnregisterSupplierCompletedDelegate, userState);
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="CentralServiceReference.IManageBuyerProposal")]
    public interface IManageBuyerProposal {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/IManageBuyerProposal/SubmitBuyerProposal", ReplyAction="http://tempuri.org/IManageBuyerProposal/SubmitBuyerProposalResponse")]
        BuyerProject.CentralServiceReference.CentralComposite SubmitBuyerProposal(BuyerProject.CentralServiceReference.ProductFamilly productFamilly, System.Uri uriBuyer);
        
        [System.ServiceModel.OperationContractAttribute(AsyncPattern=true, Action="http://tempuri.org/IManageBuyerProposal/SubmitBuyerProposal", ReplyAction="http://tempuri.org/IManageBuyerProposal/SubmitBuyerProposalResponse")]
        System.IAsyncResult BeginSubmitBuyerProposal(BuyerProject.CentralServiceReference.ProductFamilly productFamilly, System.Uri uriBuyer, System.AsyncCallback callback, object asyncState);
        
        BuyerProject.CentralServiceReference.CentralComposite EndSubmitBuyerProposal(System.IAsyncResult result);
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface IManageBuyerProposalChannel : BuyerProject.CentralServiceReference.IManageBuyerProposal, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class SubmitBuyerProposalCompletedEventArgs : System.ComponentModel.AsyncCompletedEventArgs {
        
        private object[] results;
        
        public SubmitBuyerProposalCompletedEventArgs(object[] results, System.Exception exception, bool cancelled, object userState) : 
                base(exception, cancelled, userState) {
            this.results = results;
        }
        
        public BuyerProject.CentralServiceReference.CentralComposite Result {
            get {
                base.RaiseExceptionIfNecessary();
                return ((BuyerProject.CentralServiceReference.CentralComposite)(this.results[0]));
            }
        }
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class ManageBuyerProposalClient : System.ServiceModel.ClientBase<BuyerProject.CentralServiceReference.IManageBuyerProposal>, BuyerProject.CentralServiceReference.IManageBuyerProposal {
        
        private BeginOperationDelegate onBeginSubmitBuyerProposalDelegate;
        
        private EndOperationDelegate onEndSubmitBuyerProposalDelegate;
        
        private System.Threading.SendOrPostCallback onSubmitBuyerProposalCompletedDelegate;
        
        public ManageBuyerProposalClient() {
        }
        
        public ManageBuyerProposalClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public ManageBuyerProposalClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ManageBuyerProposalClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public ManageBuyerProposalClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public event System.EventHandler<SubmitBuyerProposalCompletedEventArgs> SubmitBuyerProposalCompleted;
        
        public BuyerProject.CentralServiceReference.CentralComposite SubmitBuyerProposal(BuyerProject.CentralServiceReference.ProductFamilly productFamilly, System.Uri uriBuyer) {
            return base.Channel.SubmitBuyerProposal(productFamilly, uriBuyer);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        public System.IAsyncResult BeginSubmitBuyerProposal(BuyerProject.CentralServiceReference.ProductFamilly productFamilly, System.Uri uriBuyer, System.AsyncCallback callback, object asyncState) {
            return base.Channel.BeginSubmitBuyerProposal(productFamilly, uriBuyer, callback, asyncState);
        }
        
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Advanced)]
        public BuyerProject.CentralServiceReference.CentralComposite EndSubmitBuyerProposal(System.IAsyncResult result) {
            return base.Channel.EndSubmitBuyerProposal(result);
        }
        
        private System.IAsyncResult OnBeginSubmitBuyerProposal(object[] inValues, System.AsyncCallback callback, object asyncState) {
            BuyerProject.CentralServiceReference.ProductFamilly productFamilly = ((BuyerProject.CentralServiceReference.ProductFamilly)(inValues[0]));
            System.Uri uriBuyer = ((System.Uri)(inValues[1]));
            return this.BeginSubmitBuyerProposal(productFamilly, uriBuyer, callback, asyncState);
        }
        
        private object[] OnEndSubmitBuyerProposal(System.IAsyncResult result) {
            BuyerProject.CentralServiceReference.CentralComposite retVal = this.EndSubmitBuyerProposal(result);
            return new object[] {
                    retVal};
        }
        
        private void OnSubmitBuyerProposalCompleted(object state) {
            if ((this.SubmitBuyerProposalCompleted != null)) {
                InvokeAsyncCompletedEventArgs e = ((InvokeAsyncCompletedEventArgs)(state));
                this.SubmitBuyerProposalCompleted(this, new SubmitBuyerProposalCompletedEventArgs(e.Results, e.Error, e.Cancelled, e.UserState));
            }
        }
        
        public void SubmitBuyerProposalAsync(BuyerProject.CentralServiceReference.ProductFamilly productFamilly, System.Uri uriBuyer) {
            this.SubmitBuyerProposalAsync(productFamilly, uriBuyer, null);
        }
        
        public void SubmitBuyerProposalAsync(BuyerProject.CentralServiceReference.ProductFamilly productFamilly, System.Uri uriBuyer, object userState) {
            if ((this.onBeginSubmitBuyerProposalDelegate == null)) {
                this.onBeginSubmitBuyerProposalDelegate = new BeginOperationDelegate(this.OnBeginSubmitBuyerProposal);
            }
            if ((this.onEndSubmitBuyerProposalDelegate == null)) {
                this.onEndSubmitBuyerProposalDelegate = new EndOperationDelegate(this.OnEndSubmitBuyerProposal);
            }
            if ((this.onSubmitBuyerProposalCompletedDelegate == null)) {
                this.onSubmitBuyerProposalCompletedDelegate = new System.Threading.SendOrPostCallback(this.OnSubmitBuyerProposalCompleted);
            }
            base.InvokeAsync(this.onBeginSubmitBuyerProposalDelegate, new object[] {
                        productFamilly,
                        uriBuyer}, this.onEndSubmitBuyerProposalDelegate, this.onSubmitBuyerProposalCompletedDelegate, userState);
        }
    }
}
