﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18010
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BuyerProject.ConcreteSupplierServiceReference {
    using System.Runtime.Serialization;
    using System;
    
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ProductGroup", Namespace="http://schemas.datacontract.org/2004/07/SupplierProject.Supplier")]
    [System.SerializableAttribute()]
    public partial class ProductGroup : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private BuyerProject.ConcreteSupplierServiceReference.ProductFamilly ProductFamillyField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private BuyerProject.ConcreteSupplierServiceReference.Product[] ProductsField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public BuyerProject.ConcreteSupplierServiceReference.ProductFamilly ProductFamilly {
            get {
                return this.ProductFamillyField;
            }
            set {
                if ((this.ProductFamillyField.Equals(value) != true)) {
                    this.ProductFamillyField = value;
                    this.RaisePropertyChanged("ProductFamilly");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public BuyerProject.ConcreteSupplierServiceReference.Product[] Products {
            get {
                return this.ProductsField;
            }
            set {
                if ((object.ReferenceEquals(this.ProductsField, value) != true)) {
                    this.ProductsField = value;
                    this.RaisePropertyChanged("Products");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="ProductFamilly", Namespace="http://schemas.datacontract.org/2004/07/CentralProject")]
    public enum ProductFamilly : int {
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Hardware = 0,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Software = 1,
        
        [System.Runtime.Serialization.EnumMemberAttribute()]
        Servicos = 2,
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Runtime.Serialization", "4.0.0.0")]
    [System.Runtime.Serialization.DataContractAttribute(Name="Product", Namespace="http://schemas.datacontract.org/2004/07/SupplierProject.Supplier")]
    [System.SerializableAttribute()]
    public partial class Product : object, System.Runtime.Serialization.IExtensibleDataObject, System.ComponentModel.INotifyPropertyChanged {
        
        [System.NonSerializedAttribute()]
        private System.Runtime.Serialization.ExtensionDataObject extensionDataField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private double PriceField;
        
        [System.Runtime.Serialization.OptionalFieldAttribute()]
        private int ProductCodeField;
        
        [global::System.ComponentModel.BrowsableAttribute(false)]
        public System.Runtime.Serialization.ExtensionDataObject ExtensionData {
            get {
                return this.extensionDataField;
            }
            set {
                this.extensionDataField = value;
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public double Price {
            get {
                return this.PriceField;
            }
            set {
                if ((this.PriceField.Equals(value) != true)) {
                    this.PriceField = value;
                    this.RaisePropertyChanged("Price");
                }
            }
        }
        
        [System.Runtime.Serialization.DataMemberAttribute()]
        public int ProductCode {
            get {
                return this.ProductCodeField;
            }
            set {
                if ((this.ProductCodeField.Equals(value) != true)) {
                    this.ProductCodeField = value;
                    this.RaisePropertyChanged("ProductCode");
                }
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(ConfigurationName="ConcreteSupplierServiceReference.ISupplierContract")]
    public interface ISupplierContract {
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISupplierContract/GetDetails", ReplyAction="http://tempuri.org/ISupplierContract/GetDetailsResponse")]
        BuyerProject.ConcreteSupplierServiceReference.ProductGroup GetDetails();
        
        [System.ServiceModel.OperationContractAttribute(Action="http://tempuri.org/ISupplierContract/GetDetails", ReplyAction="http://tempuri.org/ISupplierContract/GetDetailsResponse")]
        System.Threading.Tasks.Task<BuyerProject.ConcreteSupplierServiceReference.ProductGroup> GetDetailsAsync();
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface ISupplierContractChannel : BuyerProject.ConcreteSupplierServiceReference.ISupplierContract, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class SupplierContractClient : System.ServiceModel.ClientBase<BuyerProject.ConcreteSupplierServiceReference.ISupplierContract>, BuyerProject.ConcreteSupplierServiceReference.ISupplierContract {
        
        public SupplierContractClient() {
        }
        
        public SupplierContractClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public SupplierContractClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public SupplierContractClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public SupplierContractClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public BuyerProject.ConcreteSupplierServiceReference.ProductGroup GetDetails() {
            return base.Channel.GetDetails();
        }
        
        public System.Threading.Tasks.Task<BuyerProject.ConcreteSupplierServiceReference.ProductGroup> GetDetailsAsync() {
            return base.Channel.GetDetailsAsync();
        }
    }
}
