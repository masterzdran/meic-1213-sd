﻿using System.Windows.Forms;
using BuyerProject.GUI;

namespace BuyerProject
{
    static class Program
    {
        static void Main()
        {
            Application.Run(new BuyerUi());
        }
    }
}
