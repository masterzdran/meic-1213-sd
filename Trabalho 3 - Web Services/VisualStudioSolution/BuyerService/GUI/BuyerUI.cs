﻿using System;
using System.CodeDom.Compiler;
using System.IO;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Windows.Forms;
using BuyerProject.CentralServiceReference;

namespace BuyerProject.GUI
{

    internal class TextBoxStreamWriterAdapter : TextWriter
    {
        private readonly TextBox _output;

        public TextBoxStreamWriterAdapter(TextBox output)
        {
            _output = output;
        }


        public override void Write(char value)
        {
            base.Write(value);

            _output.AppendText(value.ToString()); 
            // When character data is written, append it to the text box.
        }


        public override Encoding Encoding
        {
            get { return Encoding.UTF8; }
        }
    }

    public partial class BuyerUi : Form
    {
        private ServiceHost host;
        private ConcreteBuyerService myService;
        private ManageBuyerProposalClient central;
        private Uri myAddress;

        public BuyerUi()
        {
            InitializeComponent();

            Console.SetOut(
                new IndentedTextWriter(
                    new TextBoxStreamWriterAdapter(OutputTextBox)
                )
            );

            myAddress = new Uri("http://localhost:8123/" + Guid.NewGuid());

            myService = new ConcreteBuyerService(
                (bdata) =>
                    {
                        lock (SupplierDropdown)
                        {

                            var ctrl = new Control(bdata.Uri.AbsoluteUri);
                            if( ! SupplierDropdown.Controls.ContainsKey( bdata.Uri.AbsoluteUri ))
                            {
                                SupplierDropdown.Controls.Add(ctrl);
                            }
                        
                            var text =
                                   string.Format(
                                        "proposal id = {0}\nsupplier = {1}\nvalue = {2}", 
                                        bdata.Id, 
                                        bdata.Uri, 
                                        bdata.Value
                                    );
                        
                            DialogResult res = MessageBox.Show(text, "Do you accept this selling?",MessageBoxButtons.YesNo);

                            return (res == DialogResult.Yes);

                        }
                    }
            );

            host = new ServiceHost(myService, myAddress);
            host.Open();

            central = new ManageBuyerProposalClient();

            Console.WriteLine("Instância do Comprador no endereço={0}", myAddress);
        }
    }
}
