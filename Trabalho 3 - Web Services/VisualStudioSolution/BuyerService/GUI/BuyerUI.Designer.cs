﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.ServiceModel;
using System.Windows.Forms;
using BuyerProject.CentralServiceReference;
using BuyerProject.ConcreteSupplierServiceReference;
using ProductFamilly = BuyerProject.CentralServiceReference.ProductFamilly; 

namespace BuyerProject.GUI
{
    partial class BuyerUi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            host.Close();

            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OutputTextBox = new System.Windows.Forms.TextBox();
            this.ProdFamillyLabel = new System.Windows.Forms.Label();
            this.ProdFamillyDropdown = new System.Windows.Forms.ComboBox();
            this.SubmitAcquirementButton = new System.Windows.Forms.Button();
            this.ResponseTextLabel = new System.Windows.Forms.Label();
            this.SupPerProdFamillyTreeView = new System.Windows.Forms.TreeView();
            this.SupPerProdFamillyLabel = new System.Windows.Forms.Label();
            this.GetDetailsButton = new System.Windows.Forms.Button();
            this.SupplierDropdown = new System.Windows.Forms.ComboBox();
            this.SupplierLabel = new System.Windows.Forms.Label();
            this.ProdPerSupplierLabel = new System.Windows.Forms.Label();
            this.ProdPerSupplierTreeView = new System.Windows.Forms.TreeView();
            this.SuspendLayout();
            // 
            // OutputTextBox
            // 
            this.OutputTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.OutputTextBox.Location = new System.Drawing.Point(15, 347);
            this.OutputTextBox.Multiline = true;
            this.OutputTextBox.Name = "OutputTextBox";
            this.OutputTextBox.ReadOnly = true;
            this.OutputTextBox.Size = new System.Drawing.Size(433, 135);
            this.OutputTextBox.TabIndex = 0;
            // 
            // ProdFamillyLabel
            // 
            this.ProdFamillyLabel.AutoSize = true;
            this.ProdFamillyLabel.Location = new System.Drawing.Point(25, 22);
            this.ProdFamillyLabel.Name = "ProdFamillyLabel";
            this.ProdFamillyLabel.Size = new System.Drawing.Size(78, 13);
            this.ProdFamillyLabel.TabIndex = 1;
            this.ProdFamillyLabel.Text = "Familia de Produtos";
            // 
            // ProdFamillyDropdown
            // 
            this.ProdFamillyDropdown.FormattingEnabled = true;
            this.ProdFamillyDropdown.Location = new System.Drawing.Point(109, 21);
            this.ProdFamillyDropdown.Name = "ProdFamillyDropdown";
            this.ProdFamillyDropdown.Size = new System.Drawing.Size(184, 21);
            this.ProdFamillyDropdown.TabIndex = 2;
            this.ProdFamillyDropdown.DataSource = 
                new List<ProductFamilly>(){ProductFamilly.Servicos, ProductFamilly.Software, ProductFamilly.Hardware};
            // 
            // SubmitAcquirementButton
            // 
            this.SubmitAcquirementButton.Location = new System.Drawing.Point(331, 21);
            this.SubmitAcquirementButton.Name = "SubmitAcquirementButton";
            this.SubmitAcquirementButton.Size = new System.Drawing.Size(117, 23);
            this.SubmitAcquirementButton.TabIndex = 3;
            this.SubmitAcquirementButton.Text = "Submeter Proposta";
            this.SubmitAcquirementButton.UseVisualStyleBackColor = true;
            this.SubmitAcquirementButton.MouseClick +=
                (sender, args) =>
                {
                    try
                    {
                        lock (this)
                        {
                            central.SubmitBuyerProposalCompleted +=
                            (sender2, args2) =>
                                {
                                    try
                                    {
                                        if(args2.Error!=null)
                                        {
                                            Console.WriteLine("Submission error: " + args2.Error);
                                            return;
                                        }
                                        SupplierDropdown.Items.AddRange(args2.Result.Suppliers);
                                        Console.WriteLine(
                                            "Proposta de aquisisão submetida com o id = " + args2.Result.AcquirementProposalId
                                        );
                                
                                    }
                                    catch(FaultException<SubmissionFault> e)
                                    {
                                        Console.WriteLine("Submission error: " + e.Message);
                                    }
                                    catch (Exception e)
                                    {
                                        Console.WriteLine("Error: " + e.Message);
                                    }
                                };

                            // and call method async
                            central.SubmitBuyerProposalAsync(
                                (ProductFamilly)ProdFamillyDropdown.SelectedItem,
                                myAddress
                            );  
                        }
                        

                    }
                    catch (FaultException<SubmissionFault> e)
                    {
                        MessageBox.Show(e.Detail.Details, "Erro!!", MessageBoxButtons.OK);
                    }
                };
            // 
            // ResponseTextLabel
            // 
            this.ResponseTextLabel.AutoSize = true;
            this.ResponseTextLabel.Location = new System.Drawing.Point(12, 329);
            this.ResponseTextLabel.Name = "ResponseTextLabel";
            this.ResponseTextLabel.Size = new System.Drawing.Size(79, 13);
            this.ResponseTextLabel.TabIndex = 4;
            this.ResponseTextLabel.Text = "Response Text";
            // 
            // SupPerProdFamillyTreeView
            // 
            this.SupPerProdFamillyTreeView.Location = new System.Drawing.Point(15, 123);
            this.SupPerProdFamillyTreeView.Name = "SupPerProdFamillyTreeView";
            this.SupPerProdFamillyTreeView.Size = new System.Drawing.Size(200, 175);
            this.SupPerProdFamillyTreeView.TabIndex = 5;
            this.SupPerProdFamillyTreeView.Enabled = true;
            // 
            // SupPerProdFamillyLabel
            // 
            this.SupPerProdFamillyLabel.AutoSize = true;
            this.SupPerProdFamillyLabel.Location = new System.Drawing.Point(12, 105);
            this.SupPerProdFamillyLabel.Name = "SupPerProdFamillyLabel";
            this.SupPerProdFamillyLabel.Size = new System.Drawing.Size(142, 13);
            this.SupPerProdFamillyLabel.TabIndex = 6;
            this.SupPerProdFamillyLabel.Text = "Suppliers per Product Familly";
            // 
            // SupplierDropdown
            // 
            this.SupplierDropdown.FormattingEnabled = true;
            this.SupplierDropdown.Location = new System.Drawing.Point(109, 59);
            this.SupplierDropdown.Name = "SupplierDropdown";
            this.SupplierDropdown.Size = new System.Drawing.Size(184, 21);
            this.SupplierDropdown.TabIndex = 8;
            // 
            // GetDetailsButton
            // 
            this.GetDetailsButton.Location = new System.Drawing.Point(331, 59);
            this.GetDetailsButton.Name = "GetDetailsButton";
            this.GetDetailsButton.Size = new System.Drawing.Size(117, 23);
            this.GetDetailsButton.TabIndex = 9;
            this.GetDetailsButton.Text = "Get Details";
            this.GetDetailsButton.UseVisualStyleBackColor = true;
            this.GetDetailsButton.MouseClick +=
                async (sender, args) => // async -> makes anonymous method async
                    {
                        
                        //get selected supplier
                        var uri = (Uri)SupplierDropdown.SelectedItem;

                        if (uri==null)
                        {
                            Console.WriteLine("O URI do Fornecedor está vazio.");
                            return;
                        }

                        //create endpont conection
                        var supplier = new SupplierContractClient("SupplierService", uri.AbsoluteUri);

                        ProductGroup prods = null;
                        try
                        {
                            // and call method async
                            prods = await supplier.GetDetailsAsync();
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }

                        lock (ProdPerSupplierTreeView)
                        {
                            // update tree view
                            var parent = new TreeNode(uri.AbsoluteUri);
                            if (!ProdPerSupplierTreeView.Nodes.Contains(parent) && prods != null)
                            {
                                foreach (var prod in prods.Products)
                                {
                                    parent.Nodes.Add(
                                        string.Format("code={0} ", prod.ProductCode),
                                        string.Format("price={0} ", prod.Price)
                                    );
                                }

                                parent.Expand();
                                ProdPerSupplierTreeView.Nodes.Add(parent);
                            }
                        }
                        lock (SupPerProdFamillyTreeView)
                        {
                            // update tree view
                            var parent = new TreeNode(prods.ProductFamilly.ToString());
                            if (!SupPerProdFamillyTreeView.Nodes.Contains(parent))
                            {
                                parent.Nodes.Add(uri.AbsoluteUri );
                                parent.Expand();
                                SupPerProdFamillyTreeView.Nodes.Add(parent);
                            }

                            if(SupPerProdFamillyTreeView.Nodes.Contains(parent))
                            {
                                var res = SupPerProdFamillyTreeView.Nodes.Find(prods.ProductFamilly.ToString(), false);
                                if(res.Length>1)
                                {
                                    parent = res[0];
                                }

                                parent.Nodes.Add(uri.AbsoluteUri);
                                parent.Expand();
                            }
                        }
                    };
            // 
            // SupplierLabel
            // 
            this.SupplierLabel.AutoSize = true;
            this.SupplierLabel.Location = new System.Drawing.Point(58, 62);
            this.SupplierLabel.Name = "SupplierLabel";
            this.SupplierLabel.Size = new System.Drawing.Size(45, 13);
            this.SupplierLabel.TabIndex = 7;
            this.SupplierLabel.Text = "Supplier";
            // 
            // ProdPerSupplierLabel
            // 
            this.ProdPerSupplierLabel.AutoSize = true;
            this.ProdPerSupplierLabel.Location = new System.Drawing.Point(248, 105);
            this.ProdPerSupplierLabel.Name = "ProdPerSupplierLabel";
            this.ProdPerSupplierLabel.Size = new System.Drawing.Size(108, 13);
            this.ProdPerSupplierLabel.TabIndex = 11;
            this.ProdPerSupplierLabel.Text = "Products per Supplier";
            // 
            // ProdPerSupplierTreeView
            // 
            this.ProdPerSupplierTreeView.Location = new System.Drawing.Point(251, 123);
            this.ProdPerSupplierTreeView.Name = "ProdPerSupplierTreeView";
            this.ProdPerSupplierTreeView.Size = new System.Drawing.Size(197, 175);
            this.ProdPerSupplierTreeView.TabIndex = 10;
            this.ProdPerSupplierTreeView.Enabled = true;
            // 
            // BuyerUI
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(501, 494);
            this.Controls.Add(this.ProdPerSupplierLabel);
            this.Controls.Add(this.ProdPerSupplierTreeView);
            this.Controls.Add(this.GetDetailsButton);
            this.Controls.Add(this.SupplierDropdown);
            this.Controls.Add(this.SupplierLabel);
            this.Controls.Add(this.SupPerProdFamillyLabel);
            this.Controls.Add(this.SupPerProdFamillyTreeView);
            this.Controls.Add(this.ResponseTextLabel);
            this.Controls.Add(this.SubmitAcquirementButton);
            this.Controls.Add(this.ProdFamillyDropdown);
            this.Controls.Add(this.ProdFamillyLabel);
            this.Controls.Add(this.OutputTextBox);
            this.Name = "BuyerUI";
            this.Text = "BuyerUI";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox OutputTextBox;
        private System.Windows.Forms.Label ProdFamillyLabel;
        private System.Windows.Forms.ComboBox ProdFamillyDropdown;
        private System.Windows.Forms.Button SubmitAcquirementButton;
        private System.Windows.Forms.Label ResponseTextLabel;
        private System.Windows.Forms.TreeView SupPerProdFamillyTreeView;
        private System.Windows.Forms.Label SupPerProdFamillyLabel;
        private System.Windows.Forms.Button GetDetailsButton;
        private System.Windows.Forms.ComboBox SupplierDropdown;
        private System.Windows.Forms.Label SupplierLabel;
        private System.Windows.Forms.Label ProdPerSupplierLabel;
        private System.Windows.Forms.TreeView ProdPerSupplierTreeView;
    }
}