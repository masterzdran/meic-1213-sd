﻿using System;
using System.ServiceModel;

namespace CentralProject
{
    static class Program
    {


        static void Main()
        {
            Console.WriteLine("A lançar central...");

            using (var host = new ServiceHost(typeof(ConcreteCentralService)))
            {

                host.Open();

                Console.WriteLine("Central lançada no endereço {0}", host.BaseAddresses[0]);

                Console.WriteLine("Prima alguma tecla para sair.");

                Console.ReadKey();

            }
        }
    }
}
