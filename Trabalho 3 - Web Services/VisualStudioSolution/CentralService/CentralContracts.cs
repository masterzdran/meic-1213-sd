﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;

namespace CentralProject
{

    #region Fault Contracts

    [DataContract]
    public class SubmissionFault
    {
        [DataMember]
        public ProductFamilly SupplierFamilly { get; set; }

        [DataMember]
        public string Details { get; set; }
    }

    #endregion

    #region Service Contracts


    /// <summary>
    /// Session required methods for suppliers.
    /// </summary>
    [ServiceContract(SessionMode = SessionMode.Required, CallbackContract = typeof(ISupplier))]
    public interface IManageSupplier
    {

        /// <summary>
        /// Registing method for suppliers.
        /// </summary>
        /// <param name="productFamilly"></param>
        /// <param name="uri"> </param>
        /// <returns>new supplier ticket</returns>
        [OperationContract(IsOneWay = false, IsInitiating = true)]
        [FaultContract(typeof(SubmissionFault))]
        void RegisterSupplier(ProductFamilly productFamilly, Uri uri);

        /// <summary>
        /// Unregisting method for suppliers.
        /// </summary>
        /// <param name="productFamilly"></param>
        /// <param name="uri"> </param>
        [OperationContract(IsOneWay = false, IsInitiating = false, IsTerminating = true)]
        [FaultContract(typeof(SubmissionFault))]
        void UnregisterSupplier(ProductFamilly productFamilly, Uri uri);
    }

    /// <summary>
    /// Callback interface for suppliers.
    /// </summary>
    public interface ISupplier
    {
        /// <summary>
        /// Callback method architecture for suppliers.
        /// </summary>
        /// <param name="acquirementProposalId"> </param>
        /// <param name="uriBuyer"> </param>
        [OperationContract(IsOneWay = true)]
        void NotifyProposal(int acquirementProposalId, Uri uriBuyer);

    }

    /// <summary>
    /// Interface of the method of the central service.
    /// </summary>
    [ServiceContract]
    public interface IManageBuyerProposal
    {
       
        /// <summary>
        /// Callback method architecture for suppliers.
        /// </summary>
        /// <param name="productFamilly"></param>
        /// <param name="uriBuyer"> </param>
        [OperationContract]
        CentralComposite SubmitBuyerProposal(ProductFamilly productFamilly, Uri uriBuyer);
    }
    
    #endregion

    #region Data Contracts

    [DataContract]
    public enum ProductFamilly
    {
        [EnumMember]
        Hardware,
        [EnumMember]
        Software,
        [EnumMember]
        Servicos
    }

    [DataContract]
    public class CentralComposite
    {
        [DataMember]
        public List<Uri> Suppliers { get; set; }

        [DataMember]
        public int AcquirementProposalId { get; set; }
    }
    
    #endregion
}
