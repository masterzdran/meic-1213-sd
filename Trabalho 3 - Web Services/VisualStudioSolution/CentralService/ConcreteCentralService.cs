﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Threading;

namespace CentralProject
{
    [ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Reentrant, InstanceContextMode = InstanceContextMode.Single)]
    public class ConcreteCentralService : IManageSupplier, IManageBuyerProposal
    {

        #region SupplierFamilly ID generator

        private readonly Random _random = new Random();

        private readonly List<int> _randomsGeneratedProposals = new List<int>();

        private int NextProposalId
        {
            get
            {
                lock (_random)
                {
                    var value = _random.Next();

                    while (_randomsGeneratedProposals.Contains(value))
                    {
                        value = _random.Next();
                    }

                    _randomsGeneratedProposals.Add(value);
                    return value;
                }
            }
            set
            {
                lock (_random)
                {
                    _randomsGeneratedProposals.RemoveAll(v => v == value);
                }
            }
        }
        #endregion

        private readonly Dictionary<ProductFamilly, List<KeyValuePair<Uri, ISupplier>>> _suppliers;

        public ConcreteCentralService()
        {
            if(_suppliers==null)
            {
                _suppliers = new Dictionary<ProductFamilly, List<KeyValuePair<Uri, ISupplier>>>();
            }
        }

        #region Suppliers methods

        public void RegisterSupplier(ProductFamilly productFamilly, Uri uri)
        {
            lock (_suppliers)
            {
                try
                {

                    Console.WriteLine(
                        "Fornecedor de '{0}' na sessão = '{1}'",
                        productFamilly,
                        OperationContext.Current.SessionId
                    );

                    var callback = OperationContext.Current.GetCallbackChannel<ISupplier>();

                    if (_suppliers.ContainsKey(productFamilly))
                    {
                        _suppliers[productFamilly].Add(new KeyValuePair<Uri, ISupplier>(uri, callback));
                    }
                    else
                    {
                        _suppliers.Add(productFamilly, new List<KeyValuePair<Uri, ISupplier>> { new KeyValuePair<Uri, ISupplier>(uri, callback) });
                    }

                }
                catch (Exception e)
                {
                    var fault = new SubmissionFault
                                {
                                    Details = e.Message,
                                    SupplierFamilly = productFamilly
                                };

                    throw new FaultException<SubmissionFault>(fault);
                }
            }
        }

        public void UnregisterSupplier(ProductFamilly productFamilly, Uri uri)
        {
            lock (_suppliers)
            {
                try
                {

                    var callback = OperationContext.Current.GetCallbackChannel<ISupplier>();

                    _suppliers[productFamilly].Remove(new KeyValuePair<Uri, ISupplier>(uri, callback));

                }
                catch (Exception e)
                {
                    var fault = new SubmissionFault
                    {
                        Details = e.Message,
                        SupplierFamilly = productFamilly
                    };

                    throw new FaultException<SubmissionFault>(fault);
                }
            }
        }

        #endregion

        #region Buyers methods

        public CentralComposite SubmitBuyerProposal(ProductFamilly productFamilly, Uri buyer)
        {
            lock (_suppliers)
            {
                try
                {

                    if (_suppliers.ContainsKey(productFamilly))
                    {

                        var identifier = NextProposalId;

                        var suppliersCallbacks = new CentralComposite
                        {
                            AcquirementProposalId = identifier,
                            Suppliers = _suppliers[productFamilly].Select(supplier => supplier.Key).ToList()
                        };

                        if(_suppliers.Count==0)
                        {
                            throw new ArgumentException(
                                string.Format("suppliers available == 0")
                            );
                        }

                        foreach (var supplier in _suppliers[productFamilly])
                        {

                            //ThreadPool.QueueUserWorkItem(                                
                            //    state => supplier.Value.NotifyProposal(identifier, buyer)
                            //);

                            supplier.Value.NotifyProposal(identifier, buyer);

                        }

                        return suppliersCallbacks;
                    }

                    //
                    // familly doesn't exist...
                    //

                    throw new ArgumentException(
                        string.Format( "{0} doesn't exist.", productFamilly.ToString() ) 
                    );

                }
                catch (Exception e)
                {
                    throw new FaultException<SubmissionFault>(
                        new SubmissionFault
                            {
                                Details = e.Message,
                                SupplierFamilly = productFamilly
                            }
                    );
                }
            }
        }

        #endregion
    }
}
