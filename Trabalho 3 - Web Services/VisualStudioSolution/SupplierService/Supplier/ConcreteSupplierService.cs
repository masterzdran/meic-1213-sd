﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using SupplierProject.CentralServiceReference;
using SupplierProject.ConcreteBuyerServiceReference;
using SupplierProject.Repository;

namespace SupplierProject.Supplier
{
    [CallbackBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
    [ServiceBehavior(
        ConcurrencyMode = ConcurrencyMode.Multiple, 
        InstanceContextMode = InstanceContextMode.Single
    )]
    public class ConcreteFornecedorService : IManageSupplierCallback, ISupplierContract, IDisposable
    {
        private readonly Random _random = new Random();
        private float RandomPriceValue {
            get { return (float) ( _random.Next()*Math.PI); }
        }

        private Uri ServiceUri { get; set; }

        private ProductGroup _productGroup;
        public ProductGroup ProductGroup { get; private set; }

        private readonly ConcreteRepository _repository = new ConcreteRepository();

        ~ConcreteFornecedorService()
        {
            _repository.Store( _productGroup );
        }

        private ProductGroup GetProductGroup()
        {
            if (_productGroup != null) return _productGroup;

            _productGroup = _repository.Load();
            if(_productGroup==null)
            {
                var prods = new List<Product>();
                for (int i = 0; i < 5; ++i)
                {
                    prods.Add(new Product(i, RandomPriceValue));
                }

                var values = Enum.GetValues(typeof (ProductFamilly));
                _productGroup = new ProductGroup(
                    (ProductFamilly) values.GetValue( _random.Next(values.Length) ), 
                    prods
                );

            }
            
            return _productGroup;
        }

        public ConcreteFornecedorService()
        {
            ProductGroup = GetProductGroup();
        }

        public ConcreteFornecedorService(Uri uri)
        {
            ServiceUri = uri;
            ProductGroup = GetProductGroup();
        }

        public void Dispose()
        {
            _repository.Store(_productGroup);
        }

        public ProductGroup GetDetails()
        {   
            Console.WriteLine("A enviar detalhes...");
            return ProductGroup;
        }
        
        public void NotifyProposal(int acquirementProposalId, Uri uriBuyer)
        {
            Console.WriteLine("A efectuar uma proposta de compra...");

            var buyer = new BuyerContractClient("BuyerService", uriBuyer.AbsoluteUri);

            buyer.SubmitSellingProposalCompleted +=
                (sender, args) =>
                {
                    if (args.Error != null)
                    {
                        throw args.Error;
                    }

                    Console.WriteLine(
                        "O Comprador {0} a proposta de venda '{1}'",
                            (args.Result ? "aceitou" : "rejeitou"),
                            acquirementProposalId
                    );

                };

            buyer.SubmitSellingProposalAsync(
                new BuyerData
                {
                    Id = acquirementProposalId,
                    Uri = ServiceUri,
                    Value = RandomPriceValue
                }
            );
        }
    }
}
