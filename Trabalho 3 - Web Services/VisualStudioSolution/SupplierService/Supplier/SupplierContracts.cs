﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using SupplierProject.CentralServiceReference;

namespace SupplierProject.Supplier
{
    #region Service Contracts

    /// <summary>
    /// Method interface for suppliers.
    /// </summary>
    [ServiceContract]
    public interface ISupplierContract
    {
        [OperationContract]
        ProductGroup GetDetails();
    }

    #endregion

    #region Data Contracts

    [DataContract]
    public class Product
    {
        public Product() { } // a serialização impoe que exista um constructor sem parametros
        public Product(int prodCode, double prodPrice)
        {
            ProductCode = prodCode;
            Price = prodPrice;
        }
        [DataMember]
        public int ProductCode { get; set; }

        [DataMember]
        public double Price { get; set; }
    }

    [DataContract]
    public class ProductGroup
    {
        public ProductGroup() { } // a serialização impoe que exista um constructor sem parametros
        public ProductGroup(ProductFamilly familly, List<Product> listProducts)
        {
            Products = listProducts;
            ProductFamilly = familly;
        }

        [DataMember]
        public ProductFamilly ProductFamilly { get; set; }

        [DataMember]
        public List<Product> Products { get; set; }
    }

    #endregion
}
