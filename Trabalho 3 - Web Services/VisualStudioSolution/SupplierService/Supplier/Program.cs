﻿using System;
using System.ServiceModel;
using SupplierProject.CentralServiceReference;

namespace SupplierProject.Supplier
{
    static class Program
    {
        static void Main()
        {
            Console.WriteLine("A lançar o fornecedor...");

            var baseAddress = new Uri("http://localhost:8321/" + Guid.NewGuid() );
            var supplier1 = new ConcreteFornecedorService(baseAddress);

            using ( var host = new ServiceHost(supplier1, baseAddress) )
            {

                host.Open();

                Console.WriteLine("Fornecedor lançado no endereço={0}", baseAddress );

                var ctx = new InstanceContext( supplier1 );
                var central = new ManageSupplierClient( ctx );

                var registed = false;

                while(true)
                {
                    if ( !registed )
                    {
                        try
                        {

                            central.RegisterSupplier(
                                supplier1.ProductGroup.ProductFamilly,
                                baseAddress
                            );
                            registed = true;

                        }
                        catch(TimeoutException e)
                        {
                            Console.WriteLine(e.Message);
                        }


                    }else
                    {

                        Console.WriteLine("Para sair prima qualquer tecla.");
                        Console.ReadLine();
                        try
                        {
                            central.UnregisterSupplier(
                                supplier1.ProductGroup.ProductFamilly,
                                baseAddress
                            );

                        }
                        catch (TimeoutException e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        Console.WriteLine("Adeus!");
                        return;

                    }
                }
            }
        }
    }
}
