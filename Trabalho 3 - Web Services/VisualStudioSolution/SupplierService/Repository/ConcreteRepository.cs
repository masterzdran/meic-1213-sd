﻿using System;
using System.IO;
using System.Xml.Serialization;
using SupplierProject.CentralServiceReference;
using SupplierProject.Supplier;

namespace SupplierProject.Repository
{
    public class ConcreteRepository : IRepo<ProductGroup>
    {
        public string FilePath { get; set; }

        public ConcreteRepository()
        {
            FilePath = Directory.GetCurrentDirectory() + @"\DummyDB.xml";
        }

        public void Store(ProductGroup elem)
        {
            try
            {

                var xs = new XmlSerializer(
                            typeof(ProductGroup),
                            new[]
                            {
                                typeof(ProductFamilly), 
                                typeof(Product)
                            }
                    );

                using (var stream = new FileStream(FilePath, FileMode.Create))
                {
                    xs.Serialize(stream, elem);
                }

            }
            catch (Exception e)
            {
                Console.WriteLine("Problemas a guardar os produtos:\n{0}", e.Message);
            }

        }

        public ProductGroup Load()
        {
            try
            {
                ProductGroup products = null;

                var xs = new XmlSerializer(
                    typeof(ProductGroup),
                    new[]
                    {
                        typeof(ProductFamilly), 
                        typeof(Product)
                    }
                );

                using (var stream = new FileStream(FilePath, FileMode.Open))
                {
                    products = (ProductGroup)xs.Deserialize(stream);
                }

                return products;

            }
            catch (Exception e)
            {
                Console.WriteLine("Problemas ao carregar os produtos:\n{0}", e.Message);
            }
            return null;
        }
    }
}
