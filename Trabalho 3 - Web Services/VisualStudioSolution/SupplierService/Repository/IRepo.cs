﻿namespace SupplierProject.Repository
{
    public interface IRepo<T>
    {
        T Load();
        void Store(T suppliers);
    }
}
