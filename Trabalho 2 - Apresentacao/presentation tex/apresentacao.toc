\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\beamer@endinputifotherversion {3.24pt}
\defcounter {refsection}{0}\relax 
\select@language {portuguese}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {1}{Conte\IeC {\'u}do}{2}{0}{1}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {2}{O que \IeC {\'e} a replica\IeC {\c c}\IeC {\~a}o de dados}{3}{0}{2}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {3}{Quais s\IeC {\~a}o os problemas que este sistema pretende resolver}{4}{0}{3}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {4}{Replica\IeC {\c c}\IeC {\~a}o de Dados Otimista}{5}{0}{4}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {5}{O Algoritmo}{6}{0}{5}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {6}{Descri\IeC {\c c}\IeC {\~a}o do Algoritmo (1/2)}{7}{0}{6}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {7}{Descri\IeC {\c c}\IeC {\~a}o do Algoritmo (2/2)}{8}{0}{7}
\defcounter {refsection}{0}\relax 
\beamer@sectionintoc {8}{Exemplos de utiliza\IeC {\c c}\IeC {\~a}o}{9}{0}{8}
