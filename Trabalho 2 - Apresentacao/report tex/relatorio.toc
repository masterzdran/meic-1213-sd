\select@language {portuguese}
\contentsline {chapter}{Resumo}{i}{chapter*.1}
\contentsline {chapter}{\IeC {\'I}ndice}{iii}{chapter*.1}
\contentsline {chapter}{Introdu\IeC {\c c}\IeC {\~a}o}{1}{chapter*.3}
\contentsline {chapter}{\numberline {1}Considera\IeC {\c c}\IeC {\~o}es iniciais}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Rela\IeC {\c c}\IeC {\~a}o \emph {happens-before} entre opera\IeC {\c c}\IeC {\~o}es}{3}{section.1.1}
\contentsline {chapter}{\numberline {2}Submiss\IeC {\~a}o de Opera\IeC {\c c}\IeC {\~o}es}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}\emph {single-master}}{5}{section.2.1}
\contentsline {section}{\numberline {2.2}\emph {multi-master}}{5}{section.2.2}
\contentsline {section}{\numberline {2.3}Considera\IeC {\c c}\IeC {\~o}es sobre a submiss\IeC {\~a}o de opera\IeC {\c c}\IeC {\~o}es}{6}{section.2.3}
\contentsline {chapter}{\numberline {3}Propaga\IeC {\c c}\IeC {\~a}o da atualiza\IeC {\c c}\IeC {\~a}o}{7}{chapter.3}
\contentsline {section}{\numberline {3.1}Transfer\IeC {\^e}ncia de estado}{7}{section.3.1}
\contentsline {section}{\numberline {3.2}Transfer\IeC {\^e}ncia de opera\IeC {\c c}\IeC {\~a}o}{7}{section.3.2}
\contentsline {chapter}{\numberline {4}Agendamento de execu\IeC {\c c}\IeC {\~a}o}{9}{chapter.4}
\contentsline {section}{\numberline {4.1}Dete\IeC {\c c}\IeC {\~a}o de conflitos}{9}{section.4.1}
\contentsline {chapter}{\numberline {5}Gest\IeC {\~a}o de Conflitos}{11}{chapter.5}
\contentsline {chapter}{\numberline {6}Concluir a opera\IeC {\c c}\IeC {\~a}o}{13}{chapter.6}
\contentsline {chapter}{Conclus\IeC {\~a}o}{15}{chapter*.6}
\contentsline {chapter}{Refer\IeC {\^e}ncias}{17}{chapter*.7}
