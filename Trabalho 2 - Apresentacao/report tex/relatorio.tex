\documentclass[12pt,a4paper,titlepage,twoside,openright]{report}
\input{config/packages.tex}
\input{config/fields.tex}

\begin{document}
%------------------------------------------------------------------------------
% Inicio da Capa
%------------------------------------------------------------------------------
\input{cover/cover.tex}
%------------------------------------------------------------------------------------------------------------------------------------------------------------ 
% Fim da Capa
%------------------------------------------------------------------------------------------------------------------------------------------------------------ 

%------------------------------------------------------------------------------------------------------------------------------------------------------------ 
% Inicio da Segunda Capa
%------------------------------------------------------------------------------------------------------------------------------------------------------------ 
%\input{cover/cover2nd.tex}
%------------------------------------------------------------------------------------------------------------------------------------------------------------ 
% Fim da Capa
%------------------------------------------------------------------------------------------------------------------------------------------------------------ 

\def\thechapter{\Roman{chapter}} 
\def\thesection{\thechapter.\Roman{section}} 
\pagenumbering{roman}

%------------------------------------------------------------------------------------------------------------------------------------------------------------ 
%Agradecimentos
%------------------------------------------------------------------------------------------------------------------------------------------------------------ 
%\chapter*{Agradecimentos}
%\addcontentsline{toc}{chapter}{Agradecimentos}

%------------------------------------------------------------------------------------------------------------------------------------------------------------ 
%Resumo
%------------------------------------------------------------------------------------------------------------------------------------------------------------ 
\chapter*{Resumo}
\addcontentsline{toc}{chapter}{Resumo}
Com o objetivo de melhorar a disponibilidade e o desempenho em sistemas distribuídos, recorre-se a técnicas de replicação de dados. No entanto, quando se usam algoritmos de sincronização pessimistas (atualizações das réplicas de forma síncrona e bloqueante) obtêm-se fracas melhorias. A utilização de algoritmos pessimistas em sistemas distribuídos escaláveis leva a que no futuro o sistema fique mais lento, pois é expectável que num futuro próximo haja muitos mais clientes do que máquinas a servirem os pedidos desses clientes. Se todas as maquinas têm de se bloquear entre si, o sistema irá ficar muito lento, apesar de ter os dados sempre atualizados e várias máquinas para processar os pedidos disponíveis.

A utilização de técnicas de replicação otimistas é portanto fulcral num sistema que se quer escalável e que permita obter respostas a interrogações simples em tempo útil. As técnicas de replicação otimistas são um conjunto de algoritmos que propagam as modificações nas réplicas de forma assíncrona, detetando conflitos e estabelecendo regras de acordo (para esses conflitos) que garantam uma atualização dos dados de forma incremental. 

%------------------------------------------------------------------------------------------------------
%Indice
%-------------------------------------------------------------------------------------------------------
\cleardoublepage
\addcontentsline{toc}{chapter}{Índice}
\tableofcontents

%------------------------------------------------------------------------------------------------------ 
%Reset aos contadores
%------------------------------------------------------------------------------------------------------
\newpage
\def\thechapter       {\arabic{chapter}}
\def\thesection       {\thechapter.\arabic{section}}
\setcounter{chapter}{0}
\pagenumbering{arabic}


%------------------------------------------------------------------------------------------------------- 
%Indicie de Figuras
%-------------------------------------------------------------------------------------------------------
%\cleardoublepage
%\addcontentsline{toc}{chapter}{\listfigurename}
%\listoffigures
%-------------------------------------------------------------------------------------------------------
%Indicie de Tabelas
%-------------------------------------------------------------------------------------------------------
%\cleardoublepage
%\addcontentsline{toc}{chapter}{\listtablename}
%\listoftables
%%------------------------------------------------------------------------------------------------------
%%Indicie de Lista de Codigo
%%------------------------------------------------------------------------------------------------------
%\cleardoublepage
%\addcontentsline{toc}{chapter}{\lstlistingname}
%\lstlistoflistings

%-------------------------------------------------------------------------------------------------------
%Inicio do documento
%-------------------------------------------------------------------------------------------------------

% ------------------------------------------------------------------------------------------------------
% ------------------------------------------------------------------------------------------------------
\chapter*{Introdução}
\addcontentsline{toc}{chapter}{Introdução}
% ------------------------------------------------------------------------------------------------------
% ------------------------------------------------------------------------------------------------------
\section*{Enquadramento}

A replicação de dados é o ponto fulcral de um sistema distribuído, necessitando que o mesmo esteja (quase) sempre disponível, seja eficiente e acima de tudo consistente. A informação guardada por entre servidores replicados deve estar sincronizada para que os clientes possam aceder à mesma de qualquer local e  aceder à mesma informação.

A perspetiva tradicional do sistema é uma perspetiva mais pessimista, no ponto de vista do algoritmo, uma vez que bloqueia o acesso à informação enquanto decorre o processo de sincronização. Esta situação para além de indesejável num sistema distribuído, pois aumenta os períodos de indisponibilidade do sistema trás uma vantagem evidente, mantém o sistema síncrono e a informação consistente.

É com o intuito de melhorar o sistema distribuído reduzindo a indisponibilidade do sistema e melhorar o desempenho que surge outra perspetiva, a "Replicação Otimista"\cite{optRep}. Esta perspetiva propõe um conjunto de técnicas para ajudar a resolver os desafios que surgem com este novo paradigma, de forma a partilhar a informação.

O aspeto principal deste modelo, que é evidenciado em relação ao modelo tradicional, é a gestão dos acessos concorrentes à informação, melhorando a disponibilidade do sistema uma vez que não bloqueiam o acesso a recurso para procederem à sua total sincronização, mas delegando a sincronização dos dados para segundo plano.

Esta perspetiva trás as seguintes vantagens:
\begin{itemize}
	\item Maior disponibilidade do sistema: as atualizações de dados ocorrem em segundo plano;
	\item Permite escalar as replicas: a sincronização por entre réplicas é reduzida e feita duma forma automatizada;
\end{itemize} 

Contudo existem algumas coisas a ter em conta quando existe uma atualização assíncrona dos recursos:
\begin{itemize}
	\item Existe um período em que as replicação não são iguais, pelo que pode existir diferenças em acessos subsequentes;
	\item Caso existam conflitos nas atualizações dos dados, é necessário haver um sistema de gestão de conflitos, por forma a resolver estes conflitos e garantir que o sistema é consistente ao longo do tempo.
\end{itemize} 

Um algoritmo de "Replicação Otimista" é descrito por possuir cinco passos:
\begin{itemize}
	\item Submissão de Operações
	\item Propagação da replicação
	\item Agendamento da execução
	\item Gestão de Conflitos
	\item Concluir a operação
\end{itemize}
  
Alguns exemplos de sistemas que utilizam algoritmos de "Replicação Otimista":
\begin{itemize}
	\item DNS
	\item UseNet
	\item Bayou
	\item CVS
\end{itemize}

No restante documento, ao falarmos sobre o sistema de replicação de dados otimista, iremos usar alguns conceitos, explicados de seguida:
\begin{itemize}
	\item Objeto - É a unidade mínima de replicação;
	
	\item Réplica - É a cópia de um objeto, que é guardada num \emph{site} ou máquina;
	
	\item \emph{Site} - Um \emph{site} pode guardar replicas de múltiplos objetos. Quando estamos a descrever os algoritmos, convém separar os \emph{site}s que conseguem fazer atualizações a um objeto - chamados \emph{master} \emph{site}s - dos que são apenas réplicas de leitura.
	
	\item Operação - É uma operação auto-contida sobre um objeto. As operações são diferentes do conceito de transações das bases de dados porque as operações são propagadas e aplicadas em segundo plano, depois de terem sido submetidas pelos utilizadores.
	
	\item Propagação - Após um utilizador submeter uma operação, esta fica guardada e será propagada mais tarde sobre todos os \emph{site}s, como se fosse um vírus a espalhar-se de forma epidémica por um conjunto de máquinas.
	
%	\item Tentativa de execução e agendamento - Uma operação, inicialmente é considerada uma tentativa. Estas tentativas serão ordenadas pelos \emph{site}s até estes concordarem numa ordem de execução e num resultado final.
%	
%	\item Deteção e Resolução de conflitos - Um conflito existe quando uma pré-condição de uma operação é violada, se a sua execução for feita segundo o agendamento do sistema. Em muitos dos casos as pré-condições são colocadas implicitamente nos algoritmos de replicação. A resolução de conflitos é, geralmente, uma situação muito especifica dentro de cada aplicação.
%	
%	\item \emph{Commitment} (Confirmação) - Refere o algoritmo que é encarregue de convergir os estados das replicas nos vários \emph{site}s, aplicando um conjunto de operações depois da sua ordenação final e com todos os seus resultados dos conflitos resolvidos.
\end{itemize}

\section*{Estrutura do documento}
Este documento está organizado com a seguinte estrutura:
\begin{itemize}
	\item Capítulo \ref{initialConsiderations}: Breve introdução ao tópico deste trabalho e considerações iniciais.
	\item Capítulo \ref{operationSubmission}: Descrição dos mecanismos de submissão de operações.
	\item Capítulo \ref{replicationPropagation}: Fala sobre as diferenças entre um sistema de propagação de estado e um sistema de propagação de operação. 
	\item Capítulo \ref{executionScheduling}: Aborda o tópico do agendamento de execuções das atualizações sobre as réplicas.
	\item Capítulo \ref{conflitManagement}: Aqui é falado acerca do que pode acontecer quando, após haver um agendamento de atualizações, existem atualizações em conflito.
	\item Capítulo \ref{operationConclusion}: Quando existem algumas estratégias para resolver os conflitos é necessário haver uma gestão sobre as diferentes estratégias e depois as utilize. Neste Capítulo vamos abordar o tópico de conclusão das operações, após todos os conflitos, caso os haja, terem sido resolvidos.
\end{itemize}

% ------------------------------------------------------------------------------------------------------
% ------------------------------------------------------------------------------------------------------
\chapter{Considerações iniciais}
\label{initialConsiderations}
% ------------------------------------------------------------------------------------------------------
% ------------------------------------------------------------------------------------------------------

Num sistema de replicação existem maioritariamente duas estratégias de propagação:
\begin{itemize}

	\item \emph{Eager} (Propagação gananciosa/pessimista) - A replicação gananciosa atualiza todas as máquinas quando surge uma nova atualização sobre qualquer um dos objetos. Não existem anomalias de seriação (inconsistências) e não existe a necessidade de utilizar um sistema de reconciliação com a replicação gananciosa. Os bloqueios detetam quaisquer potenciais anomalias e convertem-nas para esperas ou \emph{deadlock}s.
	
	\item \emph{Lazy} (Propagação relaxada/otimista) - A replicação relaxada em grupo permite que cada nó atualize qualquer um dos dados locais. Quando uma transação é gravada, a transação é enviada para todos os outros nós para aplicarem as atualizações na raiz da transação às replicas do nó de destino. As marcas temporais são usadas comummente para detetar e reconciliar atualizações transacionais relaxadas em grupo

\end{itemize}
		
Dentro da estratégias de propagação otimista, temos duas categorias: Qualquer nó com a cópia dos dados pode atualiza-los (\emph{multi-master}) e cada objeto tem um nó mestre (\emph{single-master}) onde apenas o nó mestre pode atualizar a copia principal do objeto.
	
Ambas estas duas categorias de propagação otimista têm problemas com \emph{deadlock}s ou com o numero de reconciliações quando o numero de nós aumenta.

O maior objetivo num sistema de replicação otimista é o de manter as suas réplicas suficientemente semelhantes umas às outras apesar das operações que são submetidas individualmente em \emph{site}s diferentes \cite{optRep}.

Para criar um sistema de replicação de dados otimista devemos ter algumas considerações iniciais em conta. Este capítulo serve para fornecer as bases às diferentes implementações de cada um dos passos do sistema, que estão descritos nos próximos capítulos.

\section{Relação \emph{happens-before} entre operações}
Relativamente às ações que serão feitas, necessitamos de saber quais as suas relações (concorrentes ou não) e compreender o conceito de ''\emph{happens-before}'':

O agendamento requer que um sistema tenha conhecimento de quais os eventos que ocorreram e por que ordem. Contudo num sistema distribuído, onde os atrasos de comunicação são imprevisíveis, não podemos definir a ordem natural do ordenamento entre todos os eventos. O conceito \emph{happens-before} (happens-before) é uma ordenação parcial que intuitivamente captura as relações entre eventos distribuídos.

Considerando as operações \textbf{A} e \textbf{B}, submetidas nos sites \textit{I} e \textit{J}, respetivamente. A operação \textbf{A} acontece antes(\emph{happens-before}) de \textbf{B} quando:
\begin{itemize}
	\item \textit{I} é igual a \textit{J} e \textbf{A} foi submetido antes de \textbf{B};
	\item \textit{I} é diferente de \textit{J} e \textbf{B} foi submetido depois de \textit{J} ter recebido e executado \textbf{A}, ou
	\item para alguma operação \textbf{C}, \textbf{A} acontece antes (\emph{happens-before}) de \textbf{C} e \textbf{C} acontece antes (\emph{happens-before}) de \textbf{B};
\end{itemize}

Se nenhuma das operações \textbf{A} nem \textbf{B} acontecem antes uma da outra, elas dizem-se ''concorrentes''.

% ------------------------------------------------------------------------------------------------------
% ------------------------------------------------------------------------------------------------------
\chapter{Submissão de Operações}
\label{operationSubmission}
% ------------------------------------------------------------------------------------------------------
% ------------------------------------------------------------------------------------------------------

%
% Operation submission: Users submit operations at independent sites.
%

A submissão de operações é feita por um utilizador do sistema, num nó do sistema, com o intuito de fazer uma interrogação ou de provocar uma alteração de estado do sistema. Esta submissão de operações pode ser feita de duas formas, consoante o tipo de sistema que suportar um e só um nó mestre (\emph{single-master}) ou mais que um nó mestre (\emph{multi-master}).

\section{\emph{single-master}}

Num sistema \emph{single-master} (um só nó mestre), existe apenas uma replica sobre a qual são feitas as operações. As alterações, depois de efetuadas, são propagadas sobre as outras réplicas do sistema. 

Este sistema tem as seguintes vantagens:
\begin{itemize}
	\item Previne os conflitos por aceitar atualizações de um só site (mas permitir leituras de qualquer site);
	\item Simples de implementar em relação ao sistema \emph{multi-master};
	\item Escalamento de réplicas simples;
\end{itemize}

O sistema \emph{single-master} é um sistema simples mas que tem uma disponibilidade limitada, especialmente quando o sistema sofre atualizações frequentes.

\section{\emph{multi-master}}

Um sistema \emph{multi-master} (múltiplos nós mestres) permite que as atualizações sejam submetidas em múltiplas réplicas independentemente e efetua as trocas de atualizações sem o conhecimento dos utilizadores.

Este sistema tem as seguintes vantagens:
\begin{itemize}
	\item Maior disponibilidade de réplicas, face a um sistema \emph{single-master}; 
	\item Melhor suporte para ambientes dinâmicos;
	\item Mais eficiente;
\end{itemize}

Um sistema \emph{multi-master} tem os algoritmos para efetuar o agendamento de execuções e gestão de conflitos muito particulares, devido ao objetivo destes algoritmos e do sistema em si. Este sistema de submissão de operações é mais complexo que o \emph{single-master} mas permite uma maior disponibilidade de dados.

\section{Considerações sobre a submissão de operações}
O ambiente em que as operações são submetidas, para o cliente, deve ser indiferente. Ou seja, o cliente não deve notar a diferença entre submeter uma operação no site \textbf{X} ou submeter a operação no site \textbf{Y}. 

Esta afirmação é verdadeira em ambos os sistemas de submissão de operações descritos anteriormente, pois no caso do sistema \emph{single-master}, é dito que apenas um nó efetua alterações sobre os dados e os outros contêm replicas desses dados atualizados, isto não impede que a operação seja submetida em qualquer nó e posteriormente encaminhada para o nó mestre. O mesmo se aplica ao sistema \emph{multi-master}, quando a operação não é submetida num nó mestre.

% ------------------------------------------------------------------------------------------------------
% ------------------------------------------------------------------------------------------------------
\chapter{Propagação da atualização}
\label{replicationPropagation}
% ------------------------------------------------------------------------------------------------------
% ------------------------------------------------------------------------------------------------------

%
% Propagation: Each site shares the operations it knows about with the rest of the system.
%

A propagação da atualização pode ser feita de duas formas distintas:
\begin{itemize}
	\item Transferência de estado: Restringe o sistema a duas hipóteses, leituras ou reescrita de estado, com o novo estado.
	\item Transferência de operação: as operações do sistema são descritas de uma forma semântica.
\end{itemize}

A propagação das atualizações também esta muito dependente da topologia da comunicação. Com uma topologia de comunicação fixa, podemos obter um maior desempenho num ambiente onde não existe modificações, mas se mudarmos o tipo de ambiente em que a comunicação for feita, um ambiente dinâmico e com erros, é expectável que o funcionamento da topologia de comunicação fixa tenha um fraco desempenho.

Quando temos um topologia de comunicação \emph{ad-hoc}, existe uma maior adaptabilidade a um ambiente dinâmico e suscetível a erros. Com o mecanismo de propagação epidémico (como se fosse um vírus) conseguimos tirar um maior partido deste tipo de topologia de comunicação com um sistema de replicação de dados otimista. 

\section{Transferência de estado}
Este tipo de atualização de replicas restringe o sistema a duas hipóteses, leituras ou reescrita de estado, com o novo estado. O algoritmo torna-se simples, pois quando existe uma atualização sobre o estado atual e este muda, é transferido o novo estado para todas as restantes réplicas. Esta abordagem para uma estratégia de propagação é simples mas tem uma grande desvantagem quando o estado é representado por um objeto que fica demasiado grande, existe a dificuldade de manter a performance do sistema, pois haverá uma maior perda de tempo na transferência de estados entre as replicas.

Esta abordagem não permite uma boa gestão de conflitos, uma vez que a decisão será entre um estado X e um estado Y.

\section{Transferência de operação}
A transferência de operações do sistema são descritas de uma forma semântica. O sistema pode ser mais eficiente, que um sistema de transferência de estado, especialmente quando os objetos são grandes e as operações de alto nível.

Num sistema de transferência de operações existe um maior cuidado, do que num sistema de transferência de estado, pois na transferência de operações mantém-se um histórico de operações e a sua ordem. Por outro lado, o sistema de transferência de operações, é mais complexo de implementar e permite tirar proveito das propriedades semânticas tais como a comutatividade e a idempotência das operações, por forma a reduzir os conflitos e a frequência dos \emph{rollbacks}\footnote{Um \emph{rollback} é uma ação que reverte todos os efeitos causados por uma operação.}.

% ------------------------------------------------------------------------------------------------------
% ------------------------------------------------------------------------------------------------------
\chapter{Agendamento de execução}
\label{executionScheduling}
% ------------------------------------------------------------------------------------------------------
% ------------------------------------------------------------------------------------------------------

%
% Scheduling: Each site decides on an order for the operations it knows about.
%


O objetivo do agendamento é a ordenação das operações a realizar, duma forma expectável pelos utilizadores, e que permitam produzir um estado equivalente sobre as réplicas. Para este fim existem dois tipos de agendamentos: sintático e semântico.

Para sabermos que podemos efetuar determinada ordenação é necessário procurar por conflitos na ordenação das operações.

\begin{itemize}
	\item Agendamento sintático: Ordenação baseada unicamente na informação de quando, onde e quem submeteu as operações.
	\item Agendamento semântico
Este tipo de ordenação tira partido das propriedades semânticas, tais como a comutatividade e idempotência das operações, por forma a reduzir os conflitos e a frequência dos \emph{rollback}s. 
\end{itemize}

O agendamento semântico é utilizado apenas quando na definição de operações é utilizada a transferência de operações, uma vez que os sistemas em que é utilizada a transferência de estado são abstraídos das operações semânticas.

\section{Deteção de conflitos}

Uma operação A está em conflito quando a sua pré-condição não é satisfeita, dado o estado da réplica depois de ter tentado aplicar as operações anteriores a A no agendamento corrente. A gestão de conflitos envolve duas sub-tarefas: deteção de conflito e a resolução do mesmo.

Ao ser detetado um conflito, será chamado o mecanismo de resolução de conflitos que será descrito no Capítulo \ref{conflitManagement}.

A aproximação sintática utiliza o conhecimento da semântica das operações para detetar conflitos. Em alguns sistemas, o procedimento de deteção de conflitos pode já estar incorporado no próprio sistema.

As políticas semânticas são estritamente mais dispendiosas que as suas pares sintáticas, uma vez que é fácil escrever um detetor de conflitos semânticos que emula um algoritmo sintático.

A maior parte dos sistemas que utilizam transferência de operações usa um detetor semântico de conflitos, principalmente porque a aplicação já descreve as operações semanticamente - adicionar pré-condições especificas para a aplicação não requer um grande esforço. 

Sistemas que utilizam a transferência de estado podem utilizar ambas as aproximações.

% ------------------------------------------------------------------------------------------------------
% ------------------------------------------------------------------------------------------------------
\chapter{Gestão de Conflitos}
\label{conflitManagement}
% ------------------------------------------------------------------------------------------------------
% ------------------------------------------------------------------------------------------------------

%
% Conflict resolution: If there are any conflicts among the operations a site has scheduled, it must modify them in some way.
%

Os conflitos surgem quando alguma das operações falham em satisfazer as suas pré-condições.\cite{optRep}

O papel da resolução de conflitos é o de reescrever ou cancelar operações ofensivas para remover conflitos suspeitos. A resolução de conflitos pode ser manual ou automática. 

A resolução manual, simplesmente exclui a operação ofensiva do agendamento e apresenta duas versões do objeto. Cabe ao utilizador criar um objeto novo, juntar os seus dados e voltar a submeter a operação.

A resolução automática de conflitos é feita por um procedimento especifico da aplicação que recebe duas versões de um objeto e cria apenas uma. Caso este procedimento seja determinístico, pode ser sempre invocado quando é detetado um conflito.

No caso especifico do sistema Bayou\footnote{''O sistema Bayou é um sistema de armazenamento com suporte para replicação e de fraca consistência, desenhado para ambientes móveis e com uma ligação de rede problemática.''\cite{bayouBib}} há suporte para múltiplos tipos de aplicação ao definir pré-condições especificas a cada aplicação (chamadas \emph{dependecy check}) e um procedimento de resolução de conflitos (chamado de \emph{merge procedure}) para cada operação.\cite{bayouBib} Cada vez que uma operação é adicionada a um agendamento ou a ordenação do agendamento muda, o sistema Bayou executa o teste de dependências; se o teste falhar, o sistema Bayou executa o  \emph{merge procedure}, que permite resolver algum problema encontrado.

Para ajudar a prevenir os conflitos ou para fornecer um ambiente controlado quando é detetado um conflito, existem os \textbf{protocolos de compromisso} que servem principalmente três propósitos: 
\begin{enumerate}
	\item Quando os sites podem efetuar escolhas não determinísticas durante o agendamento ou a resolução de conflitos, o protocolo de compromisso garante que os sites concordam entre eles.
	\item Permite aos utilizadores saberem que operações são estáveis, por exemplo, aquelas que não têm efeitos secundários ou que de certeza que não são canceladas.
	\item O protocolo de compromisso funciona como um delimitador de espaço, porque a informação acerca das operações estáveis pode ser removida em segurança.
\end{enumerate}

Existem três tipos de protocolo de compromisso:
\begin{itemize}
	\item Compromisso implícito por conhecimento comum.
	\item Acordo em segundo plano.
	\item Compromisso por consenso - Alguns sistemas utilizam protocolos de consenso para acordarem sobre quais as operações a serem efetuadas e por que ordem.
\end{itemize}


% ------------------------------------------------------------------------------------------------------
% ------------------------------------------------------------------------------------------------------
\chapter{Concluir a operação}
\label{operationConclusion}
% ------------------------------------------------------------------------------------------------------
% ------------------------------------------------------------------------------------------------------

%
% Commitment: The sites agree on a final schedule and conflict resolution result, and the operations are made permanent.
%

%Refere o algoritmo que é encarregue de convergir os estados das replicas nos vários sites, aplicando um conjunto de operações depois da sua ordenação final e com todos os seus resultados dos conflitos resolvidos.

Uma conclusão de operação pode gerar um ou mais dados guardados que vão ser guardados ou descartados em um ou mais nós do sistema.

Um exemplo de um mecanismo que trata de conclusão de operações é o protocolo de bloqueio em duas fases (\emph{two-fase commit protocol}). O protocolo de bloqueio em duas fases consistem em:
\begin{itemize}
	\item Fase de expansão: são adquiridos todos os \emph{locks} necessários à realização da operação;
	\item Fase de encolhimento: as operações são realizadas e os \emph{locks} libertados. Nesta fase não é possível adquirir novos \emph{locks};
\end{itemize}

Este protocolo impõe a seriação sobre as maquinas envolvidas nas transações mas não impede a ocorrência de \emph{deadlocks}. Uma operação repartida por vários nós, que pode englobar mais operações, é tratada como uma só transação.

A fase anterior, de Gestão de Conflitos, ajuda o sistema quando é efetivamente necessário registar uma atualização, pois grande parte do trabalho de planeamento de quais as operações a efetuar e por que ordem, foi feito na fase anterior.

%------------------------------------------------------
%Objetivos
%------------------------------------------------------
%\chapter{Objetivos}
%-----------------------
%FIGURA
%-----------------------
%\begin{figure}[H]
%  \centering
%  \includegraphics[width=0.8\textwidth]{images/ArchSimple.png}
%  \caption{Identificação dos Componentes}
%  \label{fig:ArchSimple}
%\end{figure}
%-----------------------
%Codigo
%-----------------------
%\begin{tiny}
%\begin{lstlisting}[caption={plugin.xml: Perspectivas},label={code:plugin.xmlPerspectivas}]
%<extension point="org.Eclipse.ui.perspectives">
% <perspective
% class="pt.isel.deetc.leic.jcp4e.Eclipse.perspectives.JCP4EPerspectiveFactory"
% fixed="true"
% icon="icons/SmartCard_Icon.gif"
% id="pt.isel.deetc.leic.jcp4e.Eclipse.perspectives.jcp4e"
% name="JCP4E">
% </perspective>
%</extension>
%\end{lstlisting}
%\end{tiny}

% ------------------------------------------------------------------------------------------------------
% ------------------------------------------------------------------------------------------------------
\chapter*{Conclusão}
\addcontentsline{toc}{chapter}{Conclusão}
% ------------------------------------------------------------------------------------------------------
% ------------------------------------------------------------------------------------------------------

A conclusão mais importante que devemos tiramos com este trabalho é que não existe um algoritmo de replicação otimista ideal, que nos resolve todos os problemas. Existem sim, um conjunto de diretivas a seguir para implementar um sistema de replicação de dados otimista. A implementação em concreto é que irá ditar todos os aspetos dos sistema: performance, disponibilidade dos dados, correção dos dados e capacidade de crescimento do sistema.

Um esquema ideal de replicação procuraria atingir quatro objetivos:
\begin{enumerate}

\item Disponibilidade e escalabilidade: Fornecer uma alta disponibilidade e escalabilidade através da replicação, enquanto evita a instabilidade.

\item Mobilidade: Permitir a nós moveis ler e atualizar a base de dados enquanto desligada da rede.

\item Seriação: Fornecer um ambiente de execução de transações numa única cópia.

\item Convergência: Efetua algumas convergências para evitar que o sistema fique instável.

\end{enumerate}

Tendo em conta estes quatro aspetos, foi também possível concluir que uma solução para uma aplicação não é a solução para todas as outras. Existem particularidades que tornam cada aplicação independente e isso também se reflete nos algoritmos de replicação de dados.

% ------------------------------------------------------------------------------------------------------
% ------------------------------------------------------------------------------------------------------
%Anexos
% ------------------------------------------------------------------------------------------------------
% ------------------------------------------------------------------------------------------------------
%\begin{appendices}

%\end{appendices}

% ------------------------------------------------------------------------------------------------------
% ------------------------------------------------------------------------------------------------------
%%Referencias
% ------------------------------------------------------------------------------------------------------
% ------------------------------------------------------------------------------------------------------
\renewcommand\bibname{Referências}
\begin{thebibliography}{1} 
\addcontentsline{toc}{chapter}{Referências}
\begin{scriptsize}
	\bibitem{optRep} Yasushi Saito, Marc Shapiro - ''Optimistic Replication''
	\bibitem{highAvailLazyRep} Rivka Ladin, Barbara Liskov Liuba Shrira, Sanjay Ghemawat - ''Providing High Availability Using Lazy Replication''
	\bibitem{dangerRepAndSol} Jim Gray, Pat Helland, Patrick O'Neil, Dennis Shasha - ''The Dangers of Replication and a Solution''
	\bibitem{lazyRep} Rivka Ladin, Barbara Liskov Liuba Shrira - ''Lazy Replication: Exploiting The Semantics Of Distributed Services''
	\bibitem{icecubeApproach} Anne-Marie Kermarrec, Antony Rowstron, Marc Shapiro, Peter Druschel - ''The Icecube Approach To The Reconciliation Of Divergent Replicas''
	\bibitem{bayouBib} Douglas B. Terry, Marvin M. Theimer, Karin Petersen, Alan J. Demers,
Mike J. Spreitzer and Carl H. Hauser - ''Managing Update Conflicts in Bayou, a Weakly Connected Replicated Storage System''
\end{scriptsize}
\end{thebibliography}
\end{document}
