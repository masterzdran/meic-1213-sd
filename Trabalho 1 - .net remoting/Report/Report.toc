\select@language {portuguese}
\contentsline {chapter}{\IeC {\'I}ndice}{i}{Doc-Start}
\contentsline {chapter}{Lista de Figuras}{1}{chapter*.1}
\contentsline {chapter}{\numberline {1}Introdu\IeC {\c c}\IeC {\~a}o}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Enquadramento}{3}{section.1.1}
\contentsline {section}{\numberline {1.2}Estrutura do documento}{3}{section.1.2}
\contentsline {chapter}{\numberline {2}Objetivos}{5}{chapter.2}
\contentsline {chapter}{\numberline {3}An\IeC {\'a}lise do Sistema}{7}{chapter.3}
\contentsline {chapter}{\numberline {4}Desenvolvimento}{11}{chapter.4}
\contentsline {section}{\numberline {4.1}Arquitetura e responsabilidades das entidades}{11}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Entidade ''Client''}{11}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Entidade ''Executer''}{12}{subsection.4.1.2}
\contentsline {subsection}{\numberline {4.1.3}Entidade ''Load Balancer''}{13}{subsection.4.1.3}
\contentsline {section}{\numberline {4.2}Modo de Ativa\IeC {\c c}\IeC {\~a}o de Objetos}{14}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Modo de ativa\IeC {\c c}\IeC {\~a}o de objetos remotos}{14}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}T\IeC {\'e}cnicas de manuten\IeC {\c c}\IeC {\~a}o de estado}{15}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Mecanismos de controlo de concorr\IeC {\^e}ncia utilizados}{15}{subsection.4.2.3}
\contentsline {section}{\numberline {4.3}Toler\IeC {\^a}ncia a falhas}{15}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Falhas no ''Client''}{15}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Falhas no ''Executer''}{15}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Falhas no ''Load Balancer''}{16}{subsection.4.3.3}
\contentsline {chapter}{\numberline {5}Conclus\IeC {\~a}o}{17}{chapter.5}
\contentsline {chapter}{Conclus\IeC {\~a}o}{17}{chapter.5}
\contentsline {chapter}{Refer\IeC {\^e}ncias}{19}{chapter*.15}
