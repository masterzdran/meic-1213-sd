﻿using System.Windows.Forms;

namespace WinFormsClient
{
    partial class ConcreteGraphicClient : Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.text_output = new System.Windows.Forms.TextBox();
            this.text_input = new System.Windows.Forms.TextBox();
            this.button_send = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.backGroundWorkerForLB = new System.ComponentModel.BackgroundWorker();
            this.SuspendLayout();
            // 
            // text_output
            // 
            this.text_output.Cursor = System.Windows.Forms.Cursors.Default;
            this.text_output.Enabled = false;
            this.text_output.Location = new System.Drawing.Point(12, 12);
            this.text_output.Multiline = true;
            this.text_output.Name = "text_output";
            this.text_output.Size = new System.Drawing.Size(488, 301);
            this.text_output.TabIndex = 1;
            // 
            // text_input
            // 
            this.text_input.Cursor = System.Windows.Forms.Cursors.Default;
            this.text_input.Location = new System.Drawing.Point(12, 352);
            this.text_input.Name = "text_input";
            this.text_input.Size = new System.Drawing.Size(405, 20);
            this.text_input.TabIndex = 1;
            // 
            // button_send
            // 
            this.button_send.Cursor = System.Windows.Forms.Cursors.Default;
            this.button_send.Location = new System.Drawing.Point(423, 350);
            this.button_send.Name = "button_send";
            this.button_send.Size = new System.Drawing.Size(75, 23);
            this.button_send.TabIndex = 2;
            this.button_send.Text = "Exec";
            this.button_send.UseVisualStyleBackColor = true;
            this.button_send.Click += new System.EventHandler(this.button_send_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 336);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(237, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Command format: <exec> <arg1>,<arg2>,<argN>";
            // 
            // backGroundWorkerForLB
            // 
            this.backGroundWorkerForLB.WorkerSupportsCancellation = true;
            this.backGroundWorkerForLB.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backGroundWorkerForLB_DoWork);
            // 
            // GraphicClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(512, 385);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button_send);
            this.Controls.Add(this.text_input);
            this.Controls.Add(this.text_output);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "GraphicClient";
            this.ShowIcon = false;
            this.Text = "GraphicClient";
            this.Load += new System.EventHandler(this.ConcreteGraphicClient_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox text_output;
        private System.Windows.Forms.TextBox text_input;
        private System.Windows.Forms.Button button_send;
        private System.Windows.Forms.Label label1;
        private System.ComponentModel.BackgroundWorker backGroundWorkerForLB;
    }
}

