﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.Remoting;
using System.Threading;
using System.Windows.Forms;
using CommonTypes;

namespace WinFormsClient
{
    public partial class ConcreteGraphicClient : Form
    {
        private readonly ConcreteClient _concreteClient;

        public ConcreteGraphicClient()
        {
            RemotingConfiguration.Configure("WinFormsClient.exe.config", false);

            var clis = RemotingConfiguration.GetRegisteredWellKnownClientTypes();

            var lb = (ILoadBalancerForClient)
                        //Activator.GetObject(clis[0].ObjectType, clis[0].ObjectUrl);
                        Activator.GetObject(typeof(ILoadBalancerForClient), clis[0].ObjectUrl);

            _concreteClient = new ConcreteClient(lb, SetResponse);

            InitializeComponent();

            backGroundWorkerForLB.RunWorkerAsync();
        }

        protected void ConcreteGraphicClient_Load(object obj, EventArgs args)
        {
            text_output.AppendText("client loaded.\n\n");
        }

        private void button_send_Click(object sender, EventArgs e)
        {
            var str = text_input.Text.Trim();
            
            _concreteClient.AddRequest(str);

            text_input.Text = "";
        }

        private void SetResponse(string resp)
        {
            if (text_output.InvokeRequired)
            {
                SetTextDelegate del = SetText;
                text_output.Invoke(del, new object[] { resp });
            }
            else
            {
                SetText(resp);
            }
        }

        private delegate void SetTextDelegate(string txt);

        private void SetText(string txt)
        {
            text_output.AppendText(txt);
        }

        private void backGroundWorkerForLB_DoWork(object sender, DoWorkEventArgs e)
        {
            _concreteClient.Run();
        }
    }
}
