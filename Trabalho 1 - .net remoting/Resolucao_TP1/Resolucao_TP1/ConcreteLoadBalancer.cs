﻿using System;
using System.Collections.Generic;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Messaging;
using System.Threading;
using CommonTypes;

namespace LoadBalancerNamespace
{
    public interface ITicketGenerator
    {
        int GetTicket();
    }

    internal class ConcreteTicketGenerator : ITicketGenerator
    {
        private readonly Random _random = new Random();

        public int GetTicket()
        {
            return _random.Next();
        }
    }

    public delegate JobResponse ExecuteJobDelegate(JobRequest request);

    public class LoadBalancer : MarshalByRefObject, ILoadBalancerForClient, ILoadBalancerForExecutor
    {
        // load balancer constructors and ticket generator
        private readonly ITicketGenerator _generator = new ConcreteTicketGenerator();

        // lock for job result list
        private readonly object _jobResultLock = new object();
        private readonly Dictionary<int, string> _jobResultDicto = new Dictionary<int, string>();

        // lock for executors list
        private readonly object _executorsStackLock = new object();
        private readonly Queue<IExecutor> _executorsQueue = new Queue<IExecutor>();
        // executer -> requests // usefull in case of a executer shuts down
        //private readonly Dictionary<IExecutor, LinkedList<JobRequest>> _executorsRequests = new Dictionary<IExecutor, LinkedList<JobRequest>>();

        #region for client methods

        public int ExecuteJob(ClientRequest request)
        {
            
                int ticket = _generator.GetTicket();
                IExecutor executor;
                lock (_executorsStackLock)
                {
                    if(_executorsQueue.Count==0)
                    {
                        lock (_jobResultLock)
                        {
                            _jobResultDicto[ticket] = "LB: no executers available.";
                            return ticket;
                        }
                    }
                    executor = _executorsQueue.Dequeue();
                }

                Console.WriteLine("LB: received request="+request);

                var jobRequest = new JobRequest(ticket, request);

                try
                {

                    //if ( _executorsRequests[executor] == null || !_executorsRequests.ContainsKey(executor) )
                    //{
                    //    _executorsRequests.Add(executor, new LinkedList<JobRequest>());
                    //}

                    //_executorsRequests[executor].AddLast(jobRequest);

                    new ExecuteJobDelegate(executor.ExecuteJob)
                        .BeginInvoke(
                            jobRequest,
                            SetJobResponseCallback,
                            null//executor
                        );

                    lock (_executorsStackLock)
                    {
                        
                        
                        _executorsQueue.Enqueue(executor);
                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine("caught in BEGIN invoke. re-assigning Jobs.");

                    // re assign jobs and remove executer -> jobs reference
                    //ReAssignJobs(_executorsRequests[executor]);
                    //_executorsRequests.Remove(executor);

                    PrintExceptionInConsole(e);
                }

                return ticket;
        }

        /*
        /// <summary>
        /// in case a Executer fails, split his work for the others...
        /// </summary>
        /// <param name="jobs"></param>
        private void ReAssignJobs(IEnumerable<JobRequest> jobs)
        {
            foreach (JobRequest j in jobs)
            {
                IExecutor executor;
                lock (_executorsStackLock)
                {
                    if (_executorsQueue.Count == 0)
                    {
                        lock (_jobResultLock)
                        {
                            _jobResultDicto[j.Ticket] = "LB: no executers available.";
                            return;
                        }
                    }
                    executor = _executorsQueue.Dequeue();
                }

                try
                {

                    new ExecuteJobDelegate(executor.ExecuteJob)
                        .BeginInvoke(
                            j,
                            SetJobResponseCallback,
                            null //executor
                        );

                    lock (_executorsStackLock)
                    {
                        if (_executorsRequests[executor] == null)
                        {
                            _executorsRequests[executor] = new LinkedList<JobRequest>();
                        }
                        _executorsRequests[executor].AddLast(j);
                        _executorsQueue.Enqueue(executor);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine("caught in BEGIN invoke. re-assigning Jobs again!");

                    // re assign jobs and remove executer -> jobs reference
                    ReAssignJobs(_executorsRequests[executor]);
                    _executorsRequests.Remove(executor);

                    PrintExceptionInConsole(e);
                }
            }
        }
        */

        private void SetJobResponseCallback(IAsyncResult ar)
        {
            
            var result = (AsyncResult)ar;

            //var executer = (IExecutor)result.AsyncState;

            var caller = (ExecuteJobDelegate)result.AsyncDelegate;// Retrieve the delegate.

            try
            {
                // Call EndInvoke to retrieve the results. 
                var returnValue = caller.EndInvoke(ar);
                var ticket = returnValue.Request.Ticket;
                
                if ( ticket != -1 && !returnValue.Response.Equals(""))
                {
                    // response available
                    lock (_jobResultLock)
                    {
                        _jobResultDicto[ticket] = returnValue.Response;
                        //_executorsRequests[executer].Remove(returnValue.Request); // remove request
                        return;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("caught in END invoke");
                PrintExceptionInConsole(e);
            }

            // no response available
            //throw new DataException("Response wasn't arrived. :'(");
            Console.WriteLine("Response wasn't arrived. :'(");
        }

        public bool IsJobFinished(int token)
        {
            return _jobResultDicto.ContainsKey(token);
        }

        public void TryGetJobResult(int ticket, ref string result)
        {
            if (IsJobFinished(ticket))
            {
                lock (_jobResultLock)
                {
                    if (IsJobFinished(ticket))
                    {
                        result = _jobResultDicto[ticket];
                        _jobResultDicto.Remove(ticket);
                    }
                }
            }
        }

        #endregion

        #region for executor methods

        public void RegisterNewExecutor(IExecutor executor)
        {
            lock (_executorsStackLock)
            {
                _executorsQueue.Enqueue(executor);
            }

            Console.WriteLine("registered new executor");
        }

        public void UnregisterExecutor(IExecutor me)
        {
            lock (_executorsStackLock)
            {
                // START OF BAD AND UGLY CODE!!!
                IExecutor[] array = _executorsQueue.ToArray();
                _executorsQueue.Clear();
                foreach (IExecutor executor in array)
                {
                    if (executor != me)
                    {
                        _executorsQueue.Enqueue(executor);
                    }
                    else
                    {
                        Console.WriteLine("UNregistered executor");
                    }
                }
                // END OF BAD AND UGLY CODE!!!
            }
        }

        #endregion

        private void PrintExceptionInConsole(Exception e)
        {
            Console.WriteLine("stacktrace:\n" + e.StackTrace);
            Console.WriteLine("\nmessage = " + e.Message);
        }
    }

    #region Load Balancer launcher

    public class ConcreteLoadBalancer
    {
        private static void Main()
        {
            // read the config file and start load balancer
            RemotingConfiguration.Configure("Resolucao_TP1.exe.config", false);

            Console.WriteLine("load balancer in thread {0} is up!", Thread.CurrentThread.ManagedThreadId);

            Console.WriteLine("press Enter to exit.");
            Console.ReadLine();
        }
    }

    #endregion
}
