﻿using System;
using System.Runtime.Remoting;
using System.Threading;
using CommonTypes;

namespace ConsoleClient
{
    class ConsoleClient
    {
        private static ConcreteClient _concreteClient;

        static void Main(string[] args)
        {
            RemotingConfiguration.Configure("ConsoleClient.exe.config", false);

            var clis = RemotingConfiguration.GetRegisteredWellKnownClientTypes();

            var lb = (ILoadBalancerForClient)
                //Activator.GetObject(clis[0].ObjectType, clis[0].ObjectUrl);
                        Activator.GetObject(typeof(ILoadBalancerForClient), clis[0].ObjectUrl);

            _concreteClient = new ConcreteClient(lb, SetResponse);

            var thd = new Thread( _concreteClient.Run );
            thd.Start();

            Console.WriteLine("console client is up! write \"quit()\" to exit.");
            Console.WriteLine("usage: <cmd> <agr1>,<agr2>,<agr3>,...,<agrN>");

            var input = "";
            while (true)
            {
                input = Console.ReadLine().Trim();
                if (input.Equals("quit()")) return;
                _concreteClient.AddRequest(input);
            }

        }

        public static void SetResponse(String str)
        {
            Console.WriteLine(str);
        }
    }
}
