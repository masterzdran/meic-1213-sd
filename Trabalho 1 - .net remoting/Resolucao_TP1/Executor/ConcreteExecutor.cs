﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Runtime.Remoting;
using System.Text;
using System.Threading;
using CommonTypes;

namespace Executor
{
    class Executor : MarshalByRefObject, IExecutor
    {

        public Executor(ILoadBalancerForExecutor loadBalancer)
        {
            try
            {
                // register load balancer
                loadBalancer.RegisterNewExecutor(this);

                Console.WriteLine("created and registed executor in thread {0}",Thread.CurrentThread.ManagedThreadId);

                Console.WriteLine("press Enter to exit.");
                Console.ReadLine();

            }catch(Exception ex)
            {
                Console.Write(ex.Message);
            }finally
            {
                try
                {
                    // un register in the load balancer
                    loadBalancer.UnregisterExecutor(this);
                }
                catch (Exception ex)
                {
                    Console.Write(ex.Message);
                }
            }
        }

        public JobResponse ExecuteJob(JobRequest request)
        {
            var base_dir = GetBaseDir();
            String response = null;

            Console.WriteLine("base dir = " + base_dir);
            try
            {
                var newProc = Process.Start(
                    PrepareInfo(
                        request.ClientRequest.ExeName,
                        request.ClientRequest.Arguments.Replace(',', ' '),
                        base_dir
                    )
                );

                // redireccionamento do Standard Input (teclado) e do standard output (ecra)
                var fileInput = new StreamReader(base_dir + Path.DirectorySeparatorChar + "IOs" + Path.DirectorySeparatorChar+ "stdin.txt");
                var fileOutput = new StreamWriter(base_dir + Path.DirectorySeparatorChar + "IOs" + Path.DirectorySeparatorChar + "stdout.txt");
                var buff = new byte[2*1024];
                var memoryOutput = new StreamWriter(new MemoryStream(buff));

                // stdin.txt -> process input

                StreamWriter fileIOinput = newProc.StandardInput;

                fileInput.StreamCopy( new[] { fileIOinput } );

                fileInput.Close();
                fileIOinput.Close();

                // process output -> stdout.txt 

                StreamReader fileIOoutput = newProc.StandardOutput;

                fileIOoutput.StreamCopy( new[]{ fileOutput, memoryOutput } );

                newProc.WaitForExit();

                fileOutput.Flush();
                memoryOutput.Flush();

                response = String.Format(
                    "Executer says: {0}",
                    Encoding.Default.GetString(buff).Trim()
                );

                Console.WriteLine("Executer processed data with exit code {0} and result:\n{1}", newProc.ExitCode, response);

                //response = String.Format(
                //    "thread {0} processed exe {1} with args {2} and exit value = {3}",  
                //    Thread.CurrentThread.ManagedThreadId,
                //    request.ClientRequest.ExeName,
                //    request.ClientRequest.Arguments,
                //    newProc.ExitCode
                //);

                fileIOoutput.Close();
                fileOutput.Close();

                Console.WriteLine( 
                        "thread {0} processed exe {1} with args {2} and exit value = {3}",  
                        Thread.CurrentThread.ManagedThreadId,
                        request.ClientRequest.ExeName,
                        request.ClientRequest.Arguments,
                        newProc.ExitCode
                ); // for logging purposes

            }catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return new JobResponse(
                    request,
                    response
            );
        }

        private ProcessStartInfo PrepareInfo(
            string exe_name, 
            string args, 
            string base_dir
        )
        {
            var processSInfo = new ProcessStartInfo();

            processSInfo.WorkingDirectory = 
                base_dir + Path.DirectorySeparatorChar + "IOs";

            processSInfo.CreateNoWindow = true;
            
            processSInfo.UseShellExecute = false; 

            processSInfo.RedirectStandardInput = true;

            processSInfo.RedirectStandardOutput = true;

            processSInfo.FileName = 
                base_dir + Path.DirectorySeparatorChar + "EXEs" + Path.DirectorySeparatorChar + exe_name;
            
            processSInfo.Arguments = args;

            return processSInfo;
        }

        private String GetBaseDir()
        {
            // get current dir and go up by 2...
            var upp = Directory.GetParent(Directory.GetCurrentDirectory());
            upp = Directory.GetParent(upp.ToString());
            upp = Directory.GetParent(upp.ToString());

            return upp.ToString();
        }
    }

    public static class Extensions
    {
        public static void StreamCopy(this StreamReader fromReader, StreamWriter[] toWriter)
        {
            string line;
            while ((line = fromReader.ReadLine()) != null)
            {
                toWriter.ForEach( writer => writer.Write(line) );
            }
        }

        public static void ForEach<T>(this IEnumerable<T> enumeration, Action<T> action)
        {
            foreach (var item in enumeration)
            {
                action(item);
            }
        }

        public static void ConditionalForEach<T>(this IEnumerable<T> enumeration, Action<T> action, Func<T,bool> comparer)
        {
            foreach (var item in enumeration)
            {
                if (comparer(item)) action(item);
            }
        }
    }

    public class ConcreteExecutor
    {
        
        static void Main()
        {
            RemotingConfiguration.Configure("Executor.exe.config", false);

            var clis = RemotingConfiguration.GetRegisteredWellKnownClientTypes();

            var loadBalancer = (ILoadBalancerForExecutor)
                //Activator.GetObject(clis[0].ObjectType, clis[0].ObjectUrl);
                    Activator.GetObject(typeof(ILoadBalancerForExecutor), clis[0].ObjectUrl);

            Console.WriteLine("creating Executor in thread {0}", Thread.CurrentThread.ManagedThreadId);
            
            var x = new Executor(loadBalancer);
        }
    }


}
