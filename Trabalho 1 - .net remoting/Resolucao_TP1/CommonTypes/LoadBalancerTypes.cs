﻿using System;

namespace CommonTypes
{
    public interface ILoadBalancerForClient
    {
        /// <summary>
        /// adds current request to be processed
        /// </summary>
        /// <param name="request"></param>
        /// <returns>token that identifies this request</returns>
        int ExecuteJob(ClientRequest request);

        /// <summary>
        /// allows the client to check if the requested job is finished
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        bool IsJobFinished(int token);

        /// <summary>
        /// retrieve the request result
        /// </summary>
        /// <param name="token"></param>
        /// <param name="result">a response that the client knows who to interpret</param>
        void TryGetJobResult(int token, ref string result);
    }

    public interface ILoadBalancerForExecutor
    {
        /// <summary>
        /// register a new executor in this load balancer
        /// </summary>
        /// <param name="executor"></param>
        void RegisterNewExecutor(IExecutor executor);

        /// <summary>
        /// unregister an existing executor
        /// </summary>
        /// <param name="me"></param>
        void UnregisterExecutor(IExecutor me);
    }

    [Serializable]
    public class JobRequest
    {
        public int Ticket { get; private set; }
        public ClientRequest ClientRequest { get; private set; }
        
        public JobRequest(int ticket, ClientRequest request)
        {
            Ticket = ticket;
            ClientRequest = request;
        }
    }

    [Serializable]
    public class JobResponse
    {
        public JobRequest Request { get; private set; }
        public String Response { get; private set; }

        public JobResponse(JobRequest request, String response)
        {
            Request = request;
            Response = response;
        }
    }
}