﻿using System.Collections.Generic;

namespace CommonTypes
{
    public interface IExecutor
    {
        /// <summary>
        /// set a job for this executor to process
        /// </summary>
        /// <param name="request"></param>
        JobResponse ExecuteJob(JobRequest request);
    }
}