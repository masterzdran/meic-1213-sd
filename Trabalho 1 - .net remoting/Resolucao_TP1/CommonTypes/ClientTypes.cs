﻿using System;

namespace CommonTypes
{
    [Serializable]
    public class ClientRequest
    {
        public int Id { get; private set; }
        public string ExeName { get; private set; }
        public string Arguments { get; private set; }

        public ClientRequest(int request_id, string exe_name, string args)
        {
            Id = request_id;
            ExeName = exe_name;
            Arguments = args;
        }

        public override String ToString()
        {
            return String.Format("id={0}, exe={1}, args={2}", Id, ExeName, Arguments);
        }
    }
}
