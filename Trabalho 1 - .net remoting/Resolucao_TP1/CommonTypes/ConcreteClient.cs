using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace CommonTypes
{
    public class ConcreteClient
    {
        private volatile int _counter = 0;

        private readonly ILoadBalancerForClient _loadBalancer;

        private readonly object _requestZoneLock = new object();
        private readonly Stack<ClientRequest> _requestZone = new Stack<ClientRequest>();

        private readonly object _responseZoneLock = new object();
        // request ticket -> is completed? && value of response 
        private readonly Dictionary<int, KeyValuePair<bool, string>> _responseZone = new Dictionary<int, KeyValuePair<bool, string>>();

        public delegate void WriteResponseDelegate(String response);
        private readonly WriteResponseDelegate _responseMethod;

        public ConcreteClient(ILoadBalancerForClient loadBalancer, WriteResponseDelegate responseMethod)
        {
            _loadBalancer = loadBalancer;
            _responseMethod = responseMethod;
        }

        public void AddRequest(String str)
        {
            if (str==null) return; // do nothing on null input

            str = str.Trim();
            if (str.Equals("")) return; // do nothing on empty input

            var cut_idx = str.IndexOf(' ');
            var cmd_to_send = new[] {str.Substring(0, cut_idx), str.Substring(cut_idx)};

            if (cmd_to_send.Length > 1)
            {
                lock (_requestZoneLock)
                {

                    Console.WriteLine("CCli says: added request \"" + str + "\"");

                    _requestZone.Push(
                        new ClientRequest(
                            ++_counter,
                            cmd_to_send[0],
                            cmd_to_send[1]
                        )
                    );
                }
            }
        }

        private void SendRequests()
        {
            if (_requestZone.Count > 0)
            {
                lock (_requestZoneLock)
                {
                    lock (_responseZoneLock)
                    {
                        while (_requestZone.Count > 0)
                        {
                            var request = _requestZone.Pop();
                            Console.WriteLine("CCli says: sending request...");
                            var ticket = _loadBalancer.ExecuteJob(request);
                            _responseZone[ticket] = new KeyValuePair<bool, string>(false, "");
                            Console.WriteLine("CCli says: request sended with ticket = "+ticket);
                        }
                    }
                }
            }
        }

        private bool CheckForResponses()
        {
            bool asResponses = false;
            string response = null;

            var ticketResponse = new Dictionary<int, string>();

            lock (_responseZoneLock)
            {
                foreach (
                    var ticket in 
                    _responseZone.Keys.Where( ticket => _loadBalancer.IsJobFinished(ticket) ) // so iteramos sobre os Jobs completos
                )
                {
                    _loadBalancer.TryGetJobResult(ticket, ref response);

                    ticketResponse[ticket] = response.Trim();

                    Console.WriteLine("CCli says: received response \"" + ticketResponse[ticket] + "\"");

                    asResponses = true;
                }

                foreach (var responsePair in ticketResponse)
                {
                    _responseZone[responsePair.Key] =
                        new KeyValuePair<bool, string>( true, responsePair.Value );
                }

            } // lock end

            return asResponses;
        }

        private void WriteResponses()
        {
            if ( CheckForResponses() )
            {
                lock(_responseZoneLock)
                {

                    var toRemove = new Stack<int>();

                    foreach (var resposePair in _responseZone)
                    {
                        if (resposePair.Value.Key) // if response is here...
                        {
                            _responseMethod.Invoke(resposePair.Value.Value + "\n");
                            toRemove.Push(resposePair.Key); // remove the response
                        }
                    }

                    Array.ForEach(toRemove.ToArray(), (val) => _responseZone.Remove(val) );
                }
            }
        }

        public void Run()
        {
            while (true)
            {
                SendRequests();

                Thread.Sleep(2000);

                WriteResponses();
            }
        }
    }
}
