﻿using System;
using System.Collections;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.Remoting.Channels.Tcp;
using System.Runtime.Serialization.Formatters;
using ICalculadora;

namespace Cliente
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var clientProv = new SoapClientFormatterSinkProvider();
                var serverProv = new SoapServerFormatterSinkProvider();

                //var clientProv = new BinaryClientFormatterSinkProvider();
                //var serverProv = new BinaryServerFormatterSinkProvider
                //                     {TypeFilterLevel = System.Runtime.Serialization.Formatters.TypeFilterLevel.Full};


                // initiate connection with remote messenging server
                IDictionary props = new Hashtable();
                props["port"] = 0;

                var chnl = new HttpChannel(props, clientProv, serverProv);
                //var chnl = new TcpChannel(props, clientProv, serverProv);

                ChannelServices.RegisterChannel(chnl, false);

                ICalc robj = (ICalc)Activator.GetObject(
                                       typeof(ICalc),
                "http://localhost:1234/RemoteCalcServer.soap");

                /*ICalc robj = (ICalc)Activator.GetObject(
                                       typeof(ICalc),
                "tcp://localhost:1235/RemoteCalcServer.soap"); */

                Console.WriteLine("Vai invokar o metodo getPerson()");
                Person p = robj.GetPerson();
                Console.WriteLine("Nome da Pessoa:"+p.Nome);
                Console.WriteLine("Resultado da Soma: {0}\n", robj.Add(15, 5));
                //Console.ReadLine();

                Console.WriteLine("Resultado da Multiplicação: {0}\n", robj.Mult(15, 5));
                Console.WriteLine("valor=" + robj.GetValue());
                Console.Write("Introduza um numero inteiro: ");

                string input = Console.ReadLine();
                robj.SetValue(int.Parse(input));
                Console.WriteLine("Prima enter");
                Console.ReadLine();
                Console.WriteLine("valor=" + robj.GetValue());

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message+"\n"+ex.StackTrace);
            }

            Console.ReadLine();
        }
    }
}
