﻿using System;
using System.Security.Cryptography;

namespace ICalculadora
{
    
    [Serializable]
    public class Person
    {
        public string Nome;
        public int Cod;
    }

    public interface ICalc
    {
        int Add(int a, int b);
        int Mult(int a, int b);

        //para testes de diferenciar Singleton versus SingleCall
        void SetValue(int x);
        int GetValue();
        // para ilustrar que na interface podem ser referidas
        // outras classes Data Transfer Object (DTO) que fazem
        // parte do assembly partilhado entre cliente e servidor
        Person GetPerson();
        
    }

    [Serializable]
    public class Memory
    {
        public int Calc;
        public string Data;
    }

    public interface IMemory
    {
        Memory GetMyMemory(int id);
        void StoreMyMemory(int id, Memory data);
    }

    [Serializable]
    public class Anuncio
    {
        public string Texto;

        public string GetKey()
        {
            HashAlgorithm hash = new SHA1Managed();
            // a string texto representa o conteúdo de que se quer obter o valor de hash
            byte[] hashValue = hash.ComputeHash(System.Text.Encoding.UTF8.GetBytes(Texto));
            return new string(System.Text.Encoding.UTF8.GetChars(hashValue));
        }
    }

    public interface IAnunciar
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="texto"></param>
        /// <returns> retorna a chave para este anuncio </returns>
        string StoreAnuncio(string texto);

        string GetAnuncio(string key);

        string[] GetAllAnuncios();
    }

}
