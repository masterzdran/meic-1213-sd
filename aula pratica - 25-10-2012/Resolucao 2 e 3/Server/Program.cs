﻿using System;
using System.Collections;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.Remoting.Channels.Tcp;
using ICalculadora;

namespace Server
{

    class RemoteCalc : MarshalByRefObject, ICalc, IMemory
    {
        private object mylock = new object();
        private object plock = new object();
        private int val=50;

        public void SetValue(int x)
        {
            lock (mylock)
            {    //para testar partilha e ver efeito do Lock
                //System.Threading.Thread.Sleep(30 * 1000);

                val = x;
            }
        }
        public int GetValue()
        {
            lock (mylock)
            {
                return val;
            }
        }


        // Construtor por omissão. Pode ou não fazer sentido
        public RemoteCalc()
        {
            Console.WriteLine("Construtor de RemoteCalc");
        }
        public int Add(int op1, int op2)
        {
            return op1 + op2;
        }
        public int Mult(int op1, int op2)
        {
            return op1 * op2;
        }

        public Person GetPerson()
        {
            lock (plock)
            {
                Person p = new Person();
                p.Nome = "Jose";
                p.Cod = 1234;
                return p;
            }
          

        }
        
        ~RemoteCalc()
        {
            Console.WriteLine("Destrutor de RemoteCalc");
        }

        // -----------------
        // -- exercicio 2 --
        // -----------------

        private object _lock_memorias = new object();
        private readonly Hashtable _memorias = new Hashtable();

        public Memory GetMyMemory(int id)
        {
            lock (_lock_memorias)
            {
                if (_memorias.ContainsKey(id)) return (Memory)_memorias[id];
                return new Memory();
            }
        }

        public void StoreMyMemory(int id, Memory data)
        {
            lock (_lock_memorias)
            {
                _memorias[id] = data;
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Inicio do CalcServer");

            var clientProv = new SoapClientFormatterSinkProvider();
            var serverProv = new SoapServerFormatterSinkProvider();

            //var clientProv = new BinaryClientFormatterSinkProvider();
            //var serverProv = new BinaryServerFormatterSinkProvider();

            serverProv.TypeFilterLevel = System.Runtime.Serialization.Formatters.TypeFilterLevel.Full;

            // initiate connection with remote messenging server
            IDictionary props = new Hashtable();
            props["port"] = 1234;

            var chnl = new HttpChannel(props, clientProv, serverProv);
            //var chnl = new TcpChannel(props, clientProv, serverProv);

            ChannelServices.RegisterChannel(chnl, false);

            RemotingConfiguration.RegisterWellKnownServiceType(
                typeof(RemoteCalc),
                "RemoteCalcServer.soap",
            //WellKnownObjectMode.SingleCall); // cada pedido é servido por um novo objecto
            WellKnownObjectMode.Singleton); // pedidos servidos pelo mesmo objecto
            
            Console.WriteLine("Espera pedidos");
            Console.ReadLine();
        }
    }

}
