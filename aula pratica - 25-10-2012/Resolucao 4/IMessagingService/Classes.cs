﻿using System;

namespace IMessagingService
{

    [Serializable]
    public class Message
    {
        private string _user;
        public string User{ get { return _user; } set { _user = value; } }

        private string _data;
        public string Data { get { return _data; } set { _data = value; } }

        public Message() { }
        public Message(string usr, string msg)
        {
            _user = usr;
            _data = msg;
        }
    }

    public interface IMessengerServer {
        /// <summary>
        /// registar um cliente para receber mensagens.
        /// </summary>
        void RegistaMessageBox(String key, IMessengerClient me);

        /// <summary>
        /// enviar uma mensagem para todos os clientes. inclusive o próprio que enviou.
        /// </summary>
        /// <param name="msg"></param>
        void SendMessage(Message msg);

        /// <summary>
        /// remover um cliente do sistema de recepção de mensagens.
        /// </summary>
        void UnRegistaMessageBox(String key);
    }

    public interface IMessengerClient
    {
        /// <summary>
        /// function does what name says.
        /// </summary>
        /// <returns></returns>
        string GetUserName();

        /// <summary>
        /// does what the name says in the standard output.
        /// </summary>
        /// <param name="msg"></param>
        void WriteMessage(Message msg);
    }
}
