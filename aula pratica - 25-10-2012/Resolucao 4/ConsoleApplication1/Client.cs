﻿using System;
using System.Collections;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.Remoting.Channels.Tcp;
using System.Security.Cryptography;
using System.Threading;
using Client;
using IMessagingService;

namespace Client
{
    internal class MessengerClient : MarshalByRefObject, IMessengerClient
    {
        private IMessengerServer _remoteMsgr;
        private string _userName;
        private string _myKey;

        public MessengerClient(IMessengerServer remoteMsgr)
        {
            _remoteMsgr = remoteMsgr;
        }

        public string GetUserName()
        {
            return _userName;
        }

        // enhance this method passing him the output to send the msg
        // or a delegade reponsible to redirect output
        public void WriteMessage(Message msg)
        {
            Console.WriteLine(
                "{0} says:{1}",
                msg.User,
                msg.Data
                );
        }

        private void RegisterClient()
        {
            Random rand = new Random();

            // asks for user name
            Console.WriteLine("insert user name");
            _userName = Console.ReadLine().Trim() + rand.Next();

            HashAlgorithm hash = new SHA1Managed();
            // a string texto representa o conteúdo de que se quer obter o valor de hash
            byte[] hashValue = hash.ComputeHash(System.Text.Encoding.UTF8.GetBytes(_userName));
            _myKey = new string(System.Text.Encoding.UTF8.GetChars(hashValue));

            // register user
            _remoteMsgr.RegistaMessageBox(_myKey, this);
        }

        public void ClientCycle()
        {
            try
            {
                RegisterClient();

                Console.WriteLine("write 'exit()' to quit the program");
                // while user don't write "exit()" keep on.
                string data = "";
                while (!data.Equals("exit()"))
                {
                    // get input msg from client
                    Console.WriteLine("msg to send");
                    data = Console.ReadLine().Trim();

                    // send msg to server
                    _remoteMsgr.SendMessage(
                        new Message(_userName, data)
                        );
                }

            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
            }

            finally
            {
                try
                {
                    // un register this client to stop receiveing msgs
                    _remoteMsgr.UnRegistaMessageBox(_myKey);
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
                }
            }
        }
    }

    class Program
    {
        /// <summary>
        /// starts a console client to send and receive msgs.
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            try
            {

                var clientProv = new SoapClientFormatterSinkProvider();
                var serverProv = new SoapServerFormatterSinkProvider();

                //var clientProv = new BinaryClientFormatterSinkProvider();
                //var serverProv = new BinaryServerFormatterSinkProvider { TypeFilterLevel = System.Runtime.Serialization.Formatters.TypeFilterLevel.Full };


                // initiate connection with remote messenging server
                IDictionary props = new Hashtable();
                props["port"] = 0;

                var chnl = new HttpChannel(props, clientProv, serverProv);
                //var chnl = new TcpChannel(props, clientProv, serverProv);

                ChannelServices.RegisterChannel(chnl, false);

                // get proxy to remote singleton
                IMessengerServer remoteMsgr = (IMessengerServer) Activator.GetObject(
                    typeof (IMessengerServer),
                    "http://localhost:1234/remoteMessengerServer.soap");


                MessengerClient messengerClient = new MessengerClient(remoteMsgr);

                //Action dl = (() => messengerClient.ClientCycle());
                //IAsyncResult res = dl.BeginInvoke(
                //    (_acall) =>
                //        {
                //            Console.WriteLine("cli out!"); 
                //        } , null );

                //dl.EndInvoke(res);

                Thread th = new Thread( messengerClient.ClientCycle );
                th.Start();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message + "\n" + ex.StackTrace);
            }
        }
    }
}
