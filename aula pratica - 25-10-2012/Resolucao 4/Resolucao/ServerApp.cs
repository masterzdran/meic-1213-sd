﻿using System;
using System.Collections;
using System.Runtime.Remoting;
using System.Runtime.Remoting.Channels;
using System.Runtime.Remoting.Channels.Http;
using System.Runtime.Remoting.Channels.Tcp;
using IMessagingService;

namespace Server
{
    class MessengerServer : MarshalByRefObject, IMessengerServer
    {
        // we need this locks because this is a singleton statefull remote instance
        // lock to use when modifying the client list
        private readonly object _clientLock = new object();

        // lock to use when sending a msg over all clients
        private readonly object _msgLock = new object();

        // private state -> list of clients
        private readonly Hashtable _users = new Hashtable();

        // interface methods

        public void RegistaMessageBox(String key, IMessengerClient me)
        {
            lock (_clientLock)
            {
                if (_users.Contains( key )) return;

                _users.Add( key , me);
            }
        }

        public void SendMessage(Message msg)
        {
            lock(_msgLock)
            {
                // propagate msg to all clients
                foreach (string key in _users.Keys)
                {
                    try
                    {
                        ((IMessengerClient)_users[key]).WriteMessage(msg);
                    }catch
                    {
                        // if I can't send a msg to this guy, he goes out of the receivers!
                        UnRegistaMessageBox(key);
                    }

                }
            }
        }

        public void UnRegistaMessageBox(String key)
        {
            lock(_clientLock)
            {
                _users.Remove(key);   
            }
        }
    }

    class ServerApp
    {
        static void Main(string[] args)
        {
            // read the config file and start remote server
            //RemotingConfiguration.Configure("CSRemotingServer.exe.config", true);

            Console.WriteLine("inicio do servidor de mensagens.");

            var clientProv = new SoapClientFormatterSinkProvider();
            var serverProv = new SoapServerFormatterSinkProvider();

            //var clientProv = new BinaryClientFormatterSinkProvider();
            //var serverProv = new BinaryServerFormatterSinkProvider { TypeFilterLevel = System.Runtime.Serialization.Formatters.TypeFilterLevel.Full };


            // initiate connection with remote messenging server
            IDictionary props = new Hashtable();
            props["port"] = 0;

            var chnl = new HttpChannel(props, clientProv, serverProv);
            //var chnl = new TcpChannel(props, clientProv, serverProv);

            ChannelServices.RegisterChannel(chnl, false);

            RemotingConfiguration.RegisterWellKnownServiceType(
                typeof(MessengerServer),
                "remoteMessengerServer.soap",
                WellKnownObjectMode.Singleton
            );

            Console.WriteLine("waiting for requests...");

            Console.WriteLine("press Enter to exit.");
            Console.ReadLine();
        }
    }
}
